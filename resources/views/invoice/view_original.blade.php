@extends('layouts.admin')
@section('page-title')
    {{__('Invoice Detail')}}
@endsection
@push('css-page')
    <style>
        #card-element {
            border: 1px solid #a3afbb !important;
            border-radius: 10px !important;
            padding: 10px !important;
        }
        @media print {
    @page {
     
        margin-left: 0;
        margin-right: 0;
    }
    body {
        padding-top: 72px;
        padding-bottom: 72px ;
    }
    
header nav, footer {
display: none;
}

}
    </style>
@endpush

@push('script-page')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script type="text/javascript">
            @if($invoice->getDue() > 0  && !empty($company_payment_setting) &&  $company_payment_setting['is_stripe_enabled'] == 'on' && !empty($company_payment_setting['stripe_key']) && !empty($company_payment_setting['stripe_secret']))

        var stripe = Stripe('{{ $company_payment_setting['stripe_key'] }}');
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        var style = {
            base: {
                // Add your base input styles here. For example:
                fontSize: '14px',
                color: '#32325d',
            },
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Create a token or display an error when the form is submitted.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    $("#card-errors").html(result.error.message);
                    show_toastr('Error', result.error.message, 'error');
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }

        @endif

        @if(isset($company_payment_setting['paystack_public_key']))
        $(document).on("click", "#pay_with_paystack", function () {
            $('#paystack-payment-form').ajaxForm(function (res) {
                var amount = res.total_price;
                if (res.flag == 1) {
                    var paystack_callback = "{{ url('/invoice/paystack') }}";

                    var handler = PaystackPop.setup({
                        key: '{{ $company_payment_setting['paystack_public_key']  }}',
                        email: res.email,
                        amount: res.total_price * 100,
                        currency: res.currency,
                        ref: 'pay_ref_id' + Math.floor((Math.random() * 1000000000) +
                            1
                        ), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                        metadata: {
                            custom_fields: [{
                                display_name: "Email",
                                variable_name: "email",
                                value: res.email,
                            }]
                        },

                        callback: function (response) {

                            window.location.href = paystack_callback + '/' + response.reference + '/' + '{{encrypt($invoice->id)}}' + '?amount=' + amount;
                        },
                        onClose: function () {
                            alert('window closed');
                        }
                    });
                    handler.openIframe();
                } else if (res.flag == 2) {
                    toastrs('Error', res.msg, 'msg');
                } else {
                    toastrs('Error', res.message, 'msg');
                }

            }).submit();
        });
            @endif

            @if(isset($company_payment_setting['flutterwave_public_key']))
            //    Flaterwave Payment
            $(document).on("click", "#pay_with_flaterwave", function () {
                $('#flaterwave-payment-form').ajaxForm(function (res) {

                    if (res.flag == 1) {
                        var amount = res.total_price;
                        var API_publicKey = '{{ $company_payment_setting['flutterwave_public_key']  }}';
                        var nowTim = "{{ date('d-m-Y-h-i-a') }}";
                        var flutter_callback = "{{ url('/invoice/flaterwave') }}";
                        var x = getpaidSetup({
                            PBFPubKey: API_publicKey,
                            customer_email: '{{Auth::user()->email}}',
                            amount: res.total_price,
                            currency: '{{App\Models\Utility::getValByName('site_currency')}}',
                            txref: nowTim + '__' + Math.floor((Math.random() * 1000000000)) + 'fluttpay_online-' + '{{ date('Y-m-d') }}' + '?amount=' + amount,
                            meta: [{
                                metaname: "payment_id",
                                metavalue: "id"
                            }],
                            onclose: function () {
                            },
                            callback: function (response) {
                                var txref = response.tx.txRef;
                                if (
                                    response.tx.chargeResponseCode == "00" ||
                                    response.tx.chargeResponseCode == "0"
                                ) {
                                    window.location.href = flutter_callback + '/' + txref + '/' + '{{\Illuminate\Support\Facades\Crypt::encrypt($invoice->id)}}';
                                } else {
                                    // redirect to a failure page.
                                }
                                x.close(); // use this to close the modal immediately after payment.
                            }
                        });
                    } else if (res.flag == 2) {
                        toastrs('Error', res.msg, 'msg');
                    } else {
                        toastrs('Error', data.message, 'msg');
                    }

                }).submit();
            });
            @endif

            @if(isset($company_payment_setting['razorpay_public_key']))
            // Razorpay Payment
            $(document).on("click", "#pay_with_razorpay", function () {
                $('#razorpay-payment-form').ajaxForm(function (res) {
                    if (res.flag == 1) {
                        var amount = res.total_price;
                        var razorPay_callback = '{{url('/invoice/razorpay')}}';
                        var totalAmount = res.total_price * 100;
                        var coupon_id = res.coupon;
                        var options = {
                            "key": "{{ $company_payment_setting['razorpay_public_key']  }}", // your Razorpay Key Id
                            "amount": totalAmount,
                            "name": 'Plan',
                            "currency": '{{App\Models\Utility::getValByName('site_currency')}}',
                            "description": "",
                            "handler": function (response) {
                                window.location.href = razorPay_callback + '/' + response.razorpay_payment_id + '/' + '{{\Illuminate\Support\Facades\Crypt::encrypt($invoice->id)}}' + '?amount=' + amount;
                            },
                            "theme": {
                                "color": "#528FF0"
                            }
                        };
                        var rzp1 = new Razorpay(options);
                        rzp1.open();
                    } else if (res.flag == 2) {
                        toastrs('Error', res.msg, 'msg');
                    } else {
                        toastrs('Error', data.message, 'msg');
                    }

                }).submit();
            });
        @endif

        
        $('.cp_link').on('click', function () {
            var value = $(this).attr('data-link');
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(value).select();
            document.execCommand("copy");
            $temp.remove();
            show_toastr('Success', '{{__('Link Copy on Clipboard')}}', 'success')
        });
    </script>
    <script>
        $(document).on('click', '#shipping', function () {
            var url = $(this).data('url');
            var is_display = $("#shipping").is(":checked");
            $.ajax({
                url: url,
                type: 'get',
                data: {
                    'is_display': is_display,
                },
                success: function (data) {
                    // console.log(data);
                }
            });
        })
       
     $( document ).ready(function() {
 
 function print(){
 var divContents = $("#printdiv").html();
 var printWindow = window.open('', '', 'height=400,width=800');
 printWindow.document.write('<html><head><title>DIV Contents</title>');
 printWindow.document.write('</head><body >');
 printWindow.document.write(divContents);
 printWindow.document.write('</body></html>');
 printWindow.document.close();
 printWindow.print();
 }
});
    </script>
        
    </script>
@endpush

@section('content')


    @if(\Auth::user()->type=='company')
        @if($invoice->status!=0)
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-md-12 d-flex align-items-center justify-content-between justify-content-md-end">
                    @if(!empty($invoicePayment))
                        <div class="all-button-box mx-2">
                            <a href="#" class="btn btn-xs btn-white btn-icon-only width-auto" data-url="{{ route('invoice.credit.note',$invoice->id) }}" data-ajax-popup="true" data-title="{{__('Add Credit Note')}}">
                                {{__('Add Credit Note')}}
                            </a>
                        </div>
                    @endif
                    @if($invoice->status!=4)
                        <div class="all-button-box mx-2">
                            <a href="{{ route('invoice.payment.reminder',$invoice->id)}}" class="btn btn-xs btn-white btn-icon-only width-auto">{{__('Receipt Reminder')}}</a>
                        </div>
                    @endif
                    <div class="all-button-box mx-2">
                        <a href="{{ route('invoice.resent',$invoice->id)}}" class="btn btn-xs btn-white btn-icon-only width-auto">{{__('Resend Invoice')}}</a>
                    </div>
                    <div class="all-button-box">
                        <a href="{{ route('invoice.pdf', Crypt::encrypt($invoice->id))}}" target="_blank" class="btn btn-xs btn-white btn-icon-only width-auto">{{__('Download')}}</a>
                    </div>
                </div>
            </div>
        @endif
    @else
        <div class="row justify-content-between align-items-center mb-3">
            <div class="col-md-12 d-flex align-items-center justify-content-between justify-content-md-end">
                <div class="all-button-box mx-2">
                    <a href="#" class="btn btn-xs btn-white btn-icon-only width-auto" data-url="{{route('customer.invoice.send',$invoice->id)}}" data-ajax-popup="true" data-title="{{__('Send Invoice')}}">
                        {{__('Send Mail')}}
                    </a>
                </div>
                <div class="all-button-box mx-2">
                    <a href="{{ route('invoice.pdf', Crypt::encrypt($invoice->id))}}" target="_blank" class="btn btn-xs btn-white btn-icon-only width-auto">
                        {{__('Download')}}
                    </a>
                </div>

                @if($invoice->getDue() > 0 && !empty($company_payment_setting) && ($company_payment_setting['is_stripe_enabled'] == 'on' || $company_payment_setting['is_paypal_enabled'] == 'on' || $company_payment_setting['is_paystack_enabled'] == 'on' || $company_payment_setting['is_flutterwave_enabled'] == 'on' || $company_payment_setting['is_razorpay_enabled'] == 'on' || $company_payment_setting['is_mercado_enabled'] == 'on' || $company_payment_setting['is_paytm_enabled'] == 'on' ||
        $company_payment_setting['is_mollie_enabled']  == 'on' || $company_payment_setting['is_paypal_enabled'] == 'on' || $company_payment_setting['is_skrill_enabled'] == 'on' || $company_payment_setting['is_coingate_enabled'] == 'on'))
                    <div class="all-button-box">
                        <a href="#" class="btn btn-xs btn-white btn-icon-only width-auto" data-toggle="modal" data-target="#paymentModal">
                            {{__('Pay Now')}}
                        </a>
                    </div>
                @endif
            </div>
        </div>
    @endif

    <div class="box">
    
    <div class="box-content" >
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
               <div class="card-body">
              
                   
                     <div id="printdiv">   
                        <div class="col-md-12">
                              <div class="col-md-8" style="float: left">
                                 <table class="table table-bordered  order-table">
                                    <tr>
                                        <td>Invoice Number:</td>
                                        <td>  {{ AUth::user()->invoiceNumberFormat($invoice->invoice_id) }}</td>
                                        <td class="text-right"> {{ AUth::user()->invoiceNumberFormat($invoice->invoice_id) }}</td>
                                        <td style="direction: rtl;">رقم الفاتورة :</td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Issue Date:</td>
                                        <td>{{\Auth::user()->dateFormat($invoice->issue_date)}}</td>
                                        <td>{{\Auth::user()->dateFormat($invoice->issue_date)}}</td>
                                        <td style="direction: rtl;">تاريخ إصدار الفاتورة:</td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Due Date:</td>
                                        <td>{{\Auth::user()->dateFormat($invoice->due_date)}}</td>
                                        <td>{{\Auth::user()->dateFormat($invoice->due_date)}}</td>
                                        <td style="direction: rtl;">تاريخ استحقاق الفاتورة  :</td>
                                    </tr>
                                
                                 </table>
                              </div>
                             <div class="col-md-4" style="float: left">
                             <div class="form-group">
                                <?php $vat_no =\Auth::user()->vat_no ;?>
                                <?php $inv_date=\Auth::user()->dateFormat($invoice->issue_date) ;
                                    $grand_total=\Auth::user()->priceFormat($invoice->getSubTotal());
                                    $grand_total = ltrim($grand_total, '$');
                                    $vat_total =\Auth::user()->priceFormat($invoice->getTotal());
                                    $vat_total = ltrim($vat_total, '$');
                                ?>
                              <?php       
                               $inv_date = date("Y-m-d h:i:s", strtotime($inv_date));
                                        
                                        
                                        $seller = $customer->shipping_name;
                                        $result = chr(1) . chr( strlen($seller) ) . $seller;
                                        $result.= chr(2) . chr( strlen($vat_no) ) .$vat_no;
                                        $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                                        $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                                        $result.= chr(5) . chr( strlen($vat_total ) ) . $vat_total;
                                        $newQr = base64_encode($result); 
                                        
                                        ?>
                                    
                                    
                                    {!! QrCode::size(140)->generate($newQr);!!}
                            
                                </div>
                             </div>
                           </div>
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">{{__('Billed To')}} :</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">دفع إلى :</th>
                                            <th class="text-left" colspan="2">{{__('Shipped To')}} :</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">تم شحنها إلي:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                @if(!empty($customer->billing_name))
                                 <tr >
                                     <td>Name:</td>
                                     <td > {{!empty($customer->billing_name)?$customer->billing_name:''}}</td>
                                     <td > {{!empty($customer->billing_name_other)?$customer->billing_name_other:''}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td > {{!empty($customer->shipping_name)?$customer->shipping_name:''}}</td>
                                     <td > {{!empty($customer->shipping_name_other)?$customer->shipping_name_other:''}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                 <tr >
                                     <td>Phone:</td>
                                     <td > {{!empty($customer->billing_phone)?$customer->billing_phone:''}}</td>
                                     <td > {{!empty($customer->billing_phone)?$customer->billing_phone:''}}</td>
                                     <td style="direction: rtl;">هاتف:</td>
                                     <td>Phone:</td>
                                     <td > {{!empty($customer->shipping_phone)?$customer->shipping_phone:''}}</td>
                                     <td > {{!empty($customer->shipping_phone)?$customer->shipping_phone:''}}</td>
                                     <td style="direction: rtl;">هاتف:</td>
                                 </tr>
                                 <tr >
                                     <td>Country:</td>
                                     <td > {{!empty($customer->billing_country)?$customer->billing_country:''}}</td>
                                     <td > {{!empty($customer->billing_country_other)?$customer->billing_country_other:''}}</td>
                                     <td style="direction: rtl;">دولة:</td>
                                     <td>Country:</td>
                                     <td > {{!empty($customer->shipping_country)?$customer->shipping_country:''}}</td>
                                     <td > {{!empty($customer->shipping_country_other)?$customer->shipping_country_other:''}}</td>
                                     <td style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>State:</td>
                                     <td > {{!empty($customer->billing_state)?$customer->billing_state:''}}</td>
                                     <td > {{!empty($customer->billing_state_other)?$customer->billing_state_other:''}}</td>
                                     <td style="direction: rtl;">ولاية:</td>
                                     <td>State:</td>
                                     <td > {{!empty($customer->shipping_state)?$customer->shipping_state:''}}</td>
                                     <td > {{!empty($customer->shipping_state_other)?$customer->shipping_state_other:''}}</td>
                                     <td style="direction: rtl;">ولاية:</td>
                                 </tr>
                                 <tr >
                                     <td>City:</td>
                                     <td > {{!empty($customer->billing_city)?$customer->billing_city:''}}</td>
                                     <td > {{!empty($customer->billing_city_other)?$customer->billing_city_other:''}}</td>
                                     <td style="direction: rtl;">مدينة:</td>
                                     <td>City:</td>
                                     <td > {{!empty($customer->shipping_city)?$customer->shipping_city:''}}</td>
                                     <td > {{!empty($customer->shipping_city_other)?$customer->shipping_city_other:''}}</td>
                                     <td style="direction: rtl;">مدينة:</td>
                                 </tr>
                                <tr >
                                     <td>Address:</td>
                                     <td > {{!empty($customer->billing_address)?$customer->billing_address:''}}</td>
                                     <td > {{!empty($customer->billing_address_other)?$customer->billing_address_other:''}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                     <td>Address:</td>
                                     <td > {{!empty($customer->shipping_address)?$customer->shipping_address:''}}</td>
                                     <td > {{!empty($customer->shipping_address_other)?$customer->shipping_address_other:''}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                 </tr>
                                 <tr >
                                     <td>Zip:</td>
                                     <td > {{!empty($customer->billing_zip)?$customer->billing_zip:''}}</td>
                                     <td > {{!empty($customer->billing_zip)?$customer->billing_zip:''}}</td>
                                     <td style="direction: rtl;">أزيز:</td>
                                     <td>Zip:</td>
                                     <td > {{!empty($customer->shipping_zip)?$customer->shipping_zip:''}}</td>
                                     <td > {{!empty($customer->shipping_zip)?$customer->shipping_zip:''}}</td>
                                     <td style="direction: rtl;">أزيز:</td>
                                 </tr>
                                 @endif
                            </table>
                            <div class="row invoice-title mt-2">
                                <div class="col-xs-12 col-sm-12 col-nd-6 col-lg-6 col-12">
                                    <h4>{{__('Product')}}</h4>
                                </div>
                                
                               
                            </div>
                         <div class="col-xs-12 padding010">
                            <table class="table table-bordered">
                                   
                                <thead>
                                    <tr>
                                       <th lass="text-center font-weight-bold text-muted ">#</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Product<br>منتج </th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Quantity<br>كمية</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Rate<br>معدل</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Tax<br>ضريبة</th>
                                         <th class="text-center font-weight-bold text-muted text-uppercase">Discount<br>خصم</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Price<br>السعر </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                                $totalQuantity=0;
                                                $totalRate=0;
                                                $totalTaxPrice=0;
                                                $totalDiscount=0;
                                                $taxesData=[];
                                            @endphp
                                            @foreach($iteams as $key =>$iteam)
                                                @if(!empty($iteam->tax))
                                                    @php
                                                        $taxes=App\Models\Utility::tax($iteam->tax);
                                                        $totalQuantity+=$iteam->quantity;
                                                        $totalRate+=$iteam->price;
                                                        $totalDiscount+=$iteam->discount;
                                                        foreach($taxes as $taxe){
                                                            $taxDataPrice=App\Models\Utility::taxRate($taxe->rate,$iteam->price,$iteam->quantity);
                                                            if (array_key_exists($taxe->name,$taxesData))
                                                            {
                                                                $taxesData[$taxe->name] = $taxesData[$taxe->name]+$taxDataPrice;
                                                            }
                                                            else
                                                            {
                                                                $taxesData[$taxe->name] = $taxDataPrice;
                                                            }
                                                        }
                                                    @endphp
                                                @endif
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{!empty($iteam->product())?$iteam->product()->name:''}}</td>
                                                    <td>{{$iteam->quantity}}</td>
                                                    <td>{{\Auth::user()->priceFormat($iteam->price)}}</td>
                                                    <td>

                                                        @if(!empty($iteam->tax))
                                                           
                                                                @php $totalTaxRate = 0;@endphp
                                                                @foreach($taxes as $tax)
                                                                    @php
                                                                        $taxPrice=App\Models\Utility::taxRate($tax->rate,$iteam->price,$iteam->quantity);
                                                                        $totalTaxPrice+=$taxPrice;
                                                                    @endphp
                                                                  
                                                                       
                                                                        {{\Auth::user()->priceFormat($taxPrice)}}
                                                                   
                                                                @endforeach
                                                           
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td> @if($invoice->discount_apply==1)
                                                            {{\Auth::user()->priceFormat($iteam->discount)}}
                                                        @endif
                                                    </td>
                                                    <td class="text-right">{{\Auth::user()->priceFormat(($iteam->price*$iteam->quantity))}}</td>
                                                </tr>
                                            @endforeach
                                
                                    </tbody>
                                    <tfoot>
                                            <tr>
                                                <td></td>
                                               
                                                <td><b>{{__('Total')}}</b></td>
                                                <td><b>{{$totalQuantity}}</b></td>
                                                <td><b>{{\Auth::user()->priceFormat($totalRate)}}</b></td>
                                                <td><b>{{\Auth::user()->priceFormat($totalTaxPrice)}}</b></td>
                                                <td>  @if($invoice->discount_apply==1)
                                                        <b>{{\Auth::user()->priceFormat($totalDiscount)}}</b>
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>
                                                <td class="text-right"><b>{{__('Sub Total')}}</b></td>
                                                <td class="text-right">{{\Auth::user()->priceFormat($invoice->getSubTotal())}}</td>
                                            </tr>
                                            @if($invoice->discount_apply==1)
                                                <tr>
                                                    <td colspan="5"></td>
                                                    <td class="text-right"><b>{{__('Discount')}}</b></td>
                                                    <td class="text-right">{{\Auth::user()->priceFormat($invoice->getTotalDiscount())}}</td>
                                                </tr>
                                            @endif
                                            @if(!empty($taxesData))
                                                @foreach($taxesData as $taxName => $taxPrice)
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td class="text-right"><b>{{$taxName}}</b></td>
                                                        <td class="text-right">{{ \Auth::user()->priceFormat($taxPrice) }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            <tr>
                                                <td colspan="5"></td>
                                                <td class="blue-text text-right"><b>{{__('Total')}}</b></td>
                                                <td class="blue-text text-right">{{\Auth::user()->priceFormat($invoice->getTotal())}}</td>
                                            </tr>
                                            <!-- <tr>
                                                <td colspan="5"></td>
                                                <td class="text-right"><b>{{__('Paid')}}</b></td>
                                                <td class="text-right">{{\Auth::user()->priceFormat(($invoice->getTotal()-$invoice->getDue())-($invoice->invoiceTotalCreditNote()))}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>
                                                <td class="text-right"><b>{{__('Credit Note')}}</b></td>
                                                <td class="text-right">{{\Auth::user()->priceFormat(($invoice->invoiceTotalCreditNote()))}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>
                                                <td class="text-right"><b>{{__('Due')}}</b></td>
                                                <td class="text-right">{{\Auth::user()->priceFormat($invoice->getDue())}}</td>
                                            </tr> -->
                                            </tfoot>
                                </table>
                                </div>
                        </div>
                    </div>
                                                    </div>
                </div>
            </div>
            <div class="col-md-12"><button type="button" onclick="print()" class="btn btn-info printDivs">Print</buttom> </div>
        </div>
        
       
        
    </div>

    <script src="{{ asset('landing/js/jquery-3.4.1.min.js') }}"></script>


@endsection

