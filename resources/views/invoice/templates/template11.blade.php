<!DOCTYPE html>
<html lang="en" dir="{{env('SITE_RTL') == 'on'?'rtl':''}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

    <style type="text/css">.resize-observer[data-v-b329ee4c] {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            width: 100%;
            height: 100%;
            border: none;
            background-color: transparent;
            pointer-events: none;
            display: block;
            overflow: hidden;
            opacity: 0
        }

        .resize-observer[data-v-b329ee4c] object {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            overflow: hidden;
            pointer-events: none;
            z-index: -1
        }</style>
    <style type="text/css">p[data-v-f2a183a6] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-f2a183a6] {
            margin: 0;
        }

        .d-table[data-v-f2a183a6] {
            margin-top: 20px;
        }

        .d-table-footer[data-v-f2a183a6] {
            display: -webkit-box;
            display: flex;
        }

        .d-table-controls[data-v-f2a183a6] {
            -webkit-box-flex: 2;
            flex: 2;
        }

        .d-table-summary[data-v-f2a183a6] {
            -webkit-box-flex: 1;
            flex: 1;
        }

        .d-table-summary-item[data-v-f2a183a6] {
            width: 100%;
            display: -webkit-box;
            display: flex;
        }

        .d-table-label[data-v-f2a183a6] {
            -webkit-box-flex: 1;
            flex: 1;
            display: -webkit-box;
            display: flex;
            -webkit-box-pack: end;
            justify-content: flex-end;
            padding-top: 9px;
            padding-bottom: 9px;
        }

        .d-table-label .form-input[data-v-f2a183a6] {
            margin-left: 10px;
            width: 80px;
            height: 24px;
        }

        .d-table-label .form-input-mask-text[data-v-f2a183a6] {
            top: 3px;
        }

        .d-table-value[data-v-f2a183a6] {
            -webkit-box-flex: 1;
            flex: 1;
            text-align: right;
            padding-top: 9px;
            padding-bottom: 9px;
            padding-right: 10px;
        }

        .d-table-spacer[data-v-f2a183a6] {
            margin-top: 5px;
        }

        .d-table-tr[data-v-f2a183a6] {
            display: -webkit-box;
            display: flex;
            flex-wrap: wrap;
        }

        .d-table-td[data-v-f2a183a6] {
            padding: 10px 10px 10px 10px;
        }

        .d-table-th[data-v-f2a183a6] {
            padding: 10px 10px 10px 10px;
            font-weight: bold;
        }

        .d-body[data-v-f2a183a6] {
            padding: 50px;
        }

        .d[data-v-f2a183a6] {
            font-size: 0.9em !important;
            color: black;
            background: white;
            min-height: 1000px;
        }

        .d-right[data-v-f2a183a6] {
            text-align: right;
        }

        .d-title[data-v-f2a183a6] {
            font-size: 50px;
            line-height: 50px;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .d-header-50[data-v-f2a183a6] {
            -webkit-box-flex: 1;
            flex: 1;
        }

        .d-header-inner[data-v-f2a183a6] {
            display: -webkit-box;
            display: flex;
            padding: 50px;
        }

        .d-header-brand[data-v-f2a183a6] {
            width: 200px;
        }

        .d-logo[data-v-f2a183a6] {
            max-width: 100%;
        }</style>
    <style type="text/css">p[data-v-37eeda86] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-37eeda86] {
            margin: 0;
        }

        img[data-v-37eeda86] {
            max-width: 100%;
        }

        .d-table-value[data-v-37eeda86] {
            padding-right: 0;
        }

        .d-table-controls[data-v-37eeda86] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-37eeda86] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-e95a8a8c] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-e95a8a8c] {
            margin: 0;
        }

        img[data-v-e95a8a8c] {
            max-width: 100%;
        }

        .d[data-v-e95a8a8c] {
            font-family: monospace;
        }

        .fancy-title[data-v-e95a8a8c] {
            margin-top: 0;
            padding-top: 0;
        }

        .d-table-value[data-v-e95a8a8c] {
            padding-right: 0;
        }

        .d-table-controls[data-v-e95a8a8c] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-e95a8a8c] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-363339a0] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-363339a0] {
            margin: 0;
        }

        img[data-v-363339a0] {
            max-width: 100%;
        }

        .fancy-title[data-v-363339a0] {
            margin-top: 0;
            font-size: 30px;
            line-height: 1.2em;
            padding-top: 0;
        }

        .f-b[data-v-363339a0] {
            font-size: 17px;
            line-height: 1.2em;
        }

        .thank[data-v-363339a0] {
            font-size: 45px;
            line-height: 1.2em;
            text-align: right;
            font-style: italic;
            padding-right: 25px;
        }

        .f-remarks[data-v-363339a0] {
            padding-left: 25px;
        }

        .d-table-value[data-v-363339a0] {
            padding-right: 0;
        }

        .d-table-controls[data-v-363339a0] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-363339a0] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-e23d9750] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-e23d9750] {
            margin: 0;
        }

        img[data-v-e23d9750] {
            max-width: 100%;
        }

        .fancy-title[data-v-e23d9750] {
            margin-top: 0;
            font-size: 40px;
            line-height: 1.2em;
            font-weight: bold;
            padding: 25px;
            margin-right: 25px;
        }

        .f-b[data-v-e23d9750] {
            font-size: 17px;
            line-height: 1.2em;
        }

        .thank[data-v-e23d9750] {
            font-size: 45px;
            line-height: 1.2em;
            text-align: right;
            font-style: italic;
            padding-right: 25px;
        }

        .f-remarks[data-v-e23d9750] {
            padding: 25px;
        }

        .d-table-value[data-v-e23d9750] {
            padding-right: 0;
        }

        .d-table-controls[data-v-e23d9750] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-e23d9750] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-4b3dcb8a] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-4b3dcb8a] {
            margin: 0;
        }

        img[data-v-4b3dcb8a] {
            max-width: 100%;
        }

        .fancy-title[data-v-4b3dcb8a] {
            margin-top: 0;
            padding-top: 0;
        }

        .sub-title[data-v-4b3dcb8a] {
            margin: 5px 0 3px 0;
            display: block;
        }

        .d-table-value[data-v-4b3dcb8a] {
            padding-right: 0;
        }

        .d-table-controls[data-v-4b3dcb8a] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-4b3dcb8a] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-1ad6e3b9] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-1ad6e3b9] {
            margin: 0;
        }

        img[data-v-1ad6e3b9] {
            max-width: 100%;
        }

        .fancy-title[data-v-1ad6e3b9] {
            margin-top: 0;
            padding-top: 0;
        }

        .sub-title[data-v-1ad6e3b9] {
            margin: 5px 0 3px 0;
            display: block;
        }

        .d-no-pad[data-v-1ad6e3b9] {
            padding: 0px;
        }

        .grey-box[data-v-1ad6e3b9] {
            padding: 50px;
            background: #f8f8f8;
        }

        .d-inner-2[data-v-1ad6e3b9] {
            padding: 50px;
        }</style>
    <style type="text/css">p[data-v-136bf9b5] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-136bf9b5] {
            margin: 0;
        }

        img[data-v-136bf9b5] {
            max-width: 100%;
        }

        .fancy-title[data-v-136bf9b5] {
            margin-top: 0;
            padding-top: 0;
        }

        .d-table-value[data-v-136bf9b5] {
            padding-right: 0px;
        }</style>
    <style type="text/css">p[data-v-7d9d14b5] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-7d9d14b5] {
            margin: 0;
        }

        img[data-v-7d9d14b5] {
            max-width: 100%;
        }

        .fancy-title[data-v-7d9d14b5] {
            margin-top: 0;
            padding-top: 0;
        }

        .sub-title[data-v-7d9d14b5] {
            margin: 0 0 5px 0;
        }

        .padd[data-v-7d9d14b5] {
            margin-left: 5px;
            padding-left: 5px;
            border-left: 1px solid #f8f8f8;
            margin-right: 5px;
            padding-right: 5px;
            border-right: 1px solid #f8f8f8;
        }

        .d-inner[data-v-7d9d14b5] {
            padding-right: 0px;
        }

        .d-table-value[data-v-7d9d14b5] {
            padding-right: 5px;
        }

        .d-table-controls[data-v-7d9d14b5] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-7d9d14b5] {
            -webkit-box-flex: 4;
            flex: 4;
        }</style>
    <style type="text/css">p[data-v-b8f60a0c] {
            line-height: 1.2em;
            margin: 0 0 2px 0;
        }

        pre[data-v-b8f60a0c] {
            margin: 0;
        }

        img[data-v-b8f60a0c] {
            max-width: 100%;
        }

        .fancy-title[data-v-b8f60a0c] {
            margin-top: 0;
            padding-top: 10px;
        }

        .d-table-value[data-v-b8f60a0c] {
            padding-right: 0;
        }

        .d-table-controls[data-v-b8f60a0c] {
            -webkit-box-flex: 5;
            flex: 5;
        }

        .d-table-summary[data-v-b8f60a0c] {
            -webkit-box-flex: 4;
            flex: 4;
        }

        .overflow-x-hidden {
            overflow-x: hidden !important;
        }
        table, th, td {
  border: 1px solid black;
}

    </style>
    @if(env('SITE_RTL')=='on')
        <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.css') }}">
    @endif
</head>
<body class="">

<div class="container">
    <div id="app" class="content">
        <div class="editor">
            <div class="invoice-preview-inner">
                <div class="editor-content">
                    <div class="preview-main client-preview">
                        <div data-v-f2a183a6="" class="d" id="boxes" style="width:800px;margin-left: auto;margin-right: auto;">
                            <div data-v-f2a183a6="" class="d-header" style="background: {{$color}};color:{{$font_color}}">
                                <div data-v-f2a183a6="" class="d-header-inner">
                                    <div data-v-f2a183a6="" class="d-header-50">
                                        <div data-v-f2a183a6="" class="d-header-brand">
                                            <img src="{{$img}}" style="max-width: 250px"/>
                                        </div>
                                        <!-- <div data-v-f2a183a6="" class="break-25"></div>
                                        <p data-v-f2a183a6="">@if($settings['company_name']){{$settings['company_name']}}@endif</p>
                                        <p data-v-f2a183a6="">
                                            @if($settings['company_address']){{$settings['company_address']}}@endif
                                            @if($settings['company_city']) <br> {{$settings['company_city']}}, @endif @if($settings['company_state']){{$settings['company_state']}}@endif @if($settings['company_zipcode']) - {{$settings['company_zipcode']}}@endif
                                            @if($settings['company_country']) <br>{{$settings['company_country']}}@endif <br>
                                                @if(!empty($settings['registration_number'])){{__('Registration Number')}} : {{$settings['registration_number']}} @endif<br>
                                                @if(!empty($settings['tax_type']) && !empty($settings['vat_number'])){{$settings['tax_type'].' '. __('Number')}} : {{$settings['vat_number']}} <br>@endif

                                        </p>
                                    </div> -->

                                </div>
                            </div>
                            <div data-v-f2a183a6="" class="d-body">
                                <div data-v-f2a183a6="" class="d-bill-to">
                                    <div class="row">
                                    <table style="width:80%;border: 1px solid black;" >
                                    <tbody>
                                    <tr>
                                                    <td>{{__('Number')}}:</td>
                                                    <td>{{\App\Models\Utility::invoiceNumberFormat($settings,$invoice->invoice_id)}}</td>
                                                    <td>رقم الفاتورة</td>
                                                </tr>
                                                <tr>
                                                    <td>{{__('Issue Date')}}:</td>
                                                    <td>{{\App\Models\Utility::dateFormat($settings,$invoice->issue_date)}}</td>
                                                    <td>تاريخ إصدار الفاتورة</td>
                                                </tr>
                                                <tr>
                                                    <td>{{__('Due Date')}}:</td>
                                                    <td>{{\App\Models\Utility::dateFormat($settings,$invoice->due_date)}}</td>
                                                    <td >تاريخ استحقاق الفاتورة  :</td>
                                                </tr>
                                                <tr>
                                                    <td>{{__('VAT No')}}:</td>
                                                    <td>{{\Auth::user()->vat_no}}</td>
                                                    <td >ضريبة القيمة المضافة لا    :</td>
                                                </tr>
                                    </tbody>
                                    </table>
                                    <table  style="width:20%;">
                                        <tbody><tr><td >
                                        <?php $vat_no =\Auth::user()->vat_no ;?>
                                      <?php  $inv_date= \App\Models\Utility::dateFormat($settings,$invoice->issue_date) ;
                                             $grand_total=\App\Models\Utility::priceFormat($settings,$invoice->getSubTotal());
                                             $grand_total = ltrim($grand_total, '$');
                                             $vat_total =\Auth::user()->priceFormat($invoice->getTotal());
                                             $vat_total = ltrim($vat_total, '$');
                                ?>
                              <?php       
                                     $inv_date = date("Y-m-d h:i:s", strtotime($inv_date));
                                        
                                        
                                        $seller = $customer->shipping_name;
                                        $result = chr(1) . chr( strlen($seller) ) . $seller;
                                        $result.= chr(2) . chr( strlen($vat_no) ) .$vat_no;
                                        $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                                        $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                                        $result.= chr(5) . chr( strlen($vat_total ) ) . $vat_total;
                                        $newQr = base64_encode($result); 
                                       // echo "<pre>";print_r($result);die;
                                        ?>
                                   
                                     <center> {!! DNS2D::getBarcodeHTML($newQr, 'QRCODE',3,3) !!} </center>
                                   <!-- <center> {!! QrCode::size(100)->generate($newQr);!!}</center> -->
                                        
                                        <!-- {!! DNS2D::getBarcodeHTML(route('pay.invoice',\Illuminate\Support\Facades\Crypt::encrypt($invoice->id)), "QRCODE",2,2) !!}</td></tr></tbody> -->
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <div data-v-f2a183a6="" class="d-body">
                                <div data-v-f2a183a6="" class="d-bill-to">
                                    <div class="row">
                                    <table style="width:100%;border: 1px solid black;" >
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">{{__('Billed To')}} :</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">دفع إلى :</th>
                                            <th class="text-left" colspan="2">{{__('Shipped To')}} :</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">تم شحنها إلي:</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                @if(!empty($customer->billing_name))
                                 <tr >
                                     <td>Name:</td>
                                     <td > {{!empty($customer->billing_name)?$customer->billing_name:''}}</td>
                                     <td > {{!empty($customer->billing_name_other)?$customer->billing_name_other:''}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td > {{!empty($customer->shipping_name)?$customer->shipping_name:''}}</td>
                                     <td > {{!empty($customer->shipping_name_other)?$customer->shipping_name_other:''}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                 <tr >
                                     <td>Phone:</td>
                                     <td > {{!empty($customer->billing_phone)?$customer->billing_phone:''}}</td>
                                     <td > {{!empty($customer->billing_phone)?$customer->billing_phone:''}}</td>
                                     <td style="direction: rtl;">هاتف:</td>
                                     <td>Phone:</td>
                                     <td > {{!empty($customer->shipping_phone)?$customer->shipping_phone:''}}</td>
                                     <td > {{!empty($customer->shipping_phone)?$customer->shipping_phone:''}}</td>
                                     <td style="direction: rtl;">هاتف:</td>
                                 </tr>
                                 <tr >
                                     <td>Country:</td>
                                     <td > {{!empty($customer->billing_country)?$customer->billing_country:''}}</td>
                                     <td > {{!empty($customer->billing_country_other)?$customer->billing_country_other:''}}</td>
                                     <td style="direction: rtl;">دولة:</td>
                                     <td>Country:</td>
                                     <td > {{!empty($customer->shipping_country)?$customer->shipping_country:''}}</td>
                                     <td > {{!empty($customer->shipping_country_other)?$customer->shipping_country_other:''}}</td>
                                     <td style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>State:</td>
                                     <td > {{!empty($customer->billing_state)?$customer->billing_state:''}}</td>
                                     <td > {{!empty($customer->billing_state_other)?$customer->billing_state_other:''}}</td>
                                     <td style="direction: rtl;">ولاية:</td>
                                     <td>State:</td>
                                     <td > {{!empty($customer->shipping_state)?$customer->shipping_state:''}}</td>
                                     <td > {{!empty($customer->shipping_state_other)?$customer->shipping_state_other:''}}</td>
                                     <td style="direction: rtl;">ولاية:</td>
                                 </tr>
                                 <tr >
                                     <td>City:</td>
                                     <td > {{!empty($customer->billing_city)?$customer->billing_city:''}}</td>
                                     <td > {{!empty($customer->billing_city_other)?$customer->billing_city_other:''}}</td>
                                     <td style="direction: rtl;">مدينة:</td>
                                     <td>City:</td>
                                     <td > {{!empty($customer->shipping_city)?$customer->shipping_city:''}}</td>
                                     <td > {{!empty($customer->shipping_city_other)?$customer->shipping_city_other:''}}</td>
                                     <td style="direction: rtl;">مدينة:</td>
                                 </tr>
                                <tr >
                                     <td>Address:</td>
                                     <td > {{!empty($customer->billing_address)?$customer->billing_address:''}}</td>
                                     <td > {{!empty($customer->billing_address_other)?$customer->billing_address_other:''}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                     <td>Address:</td>
                                     <td > {{!empty($customer->shipping_address)?$customer->shipping_address:''}}</td>
                                     <td > {{!empty($customer->shipping_address_other)?$customer->shipping_address_other:''}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                 </tr>
                                 <tr >
                                     <td>Zip:</td>
                                     <td > {{!empty($customer->billing_zip)?$customer->billing_zip:''}}</td>
                                     <td > {{!empty($customer->billing_zip)?$customer->billing_zip:''}}</td>
                                     <td style="direction: rtl;">أزيز:</td>
                                     <td>Zip:</td>
                                     <td > {{!empty($customer->shipping_zip)?$customer->shipping_zip:''}}</td>
                                     <td > {{!empty($customer->shipping_zip)?$customer->shipping_zip:''}}</td>
                                     <td style="direction: rtl;">أزيز:</td>
                                 </tr>
                                 @endif
                            </table>
                                   
                                      
                                    </div>
                                    <div data-v-f2a183a6="" class="d-table">
                                        <div data-v-f2a183a6="" class="d-table">
                                        <table class="table table-bordered">
                                         <thead>
                                            <tr>
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Product<br>منتج </th>
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Quantity<br>كمية</th>
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Rate<br>معدل</th>
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Tax<br>ضريبة</th>
                                                @if($invoice->discount_apply==1)
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Discount<br>خصم</th>
                                                @else
                                                <th  class="text-center font-weight-bold text-muted text-uppercase"></th>
                                                @endif
                                                <th  class="text-center font-weight-bold text-muted text-uppercase">{{__('Description')}}<br>وصف</th>
                                                <th class="text-center font-weight-bold text-muted text-uppercase">Price<br>السعر </th>
                                           </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($invoice->itemData) && count($invoice->itemData) > 0)
                                                    @foreach($invoice->itemData as $key => $item)
                                                    <tr>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{$item->quantity}}</td>
                                                        <td>{{\App\Models\Utility::priceFormat($settings,$item->price)}}</td>
                                                        <td> @if(!empty($item->itemTax))
                                                                        @foreach($item->itemTax as $taxes)
                                                                            <span>{{$taxes['name']}}</span>  <span>({{$taxes['rate']}})</span> <span>{{$taxes['price']}}</span>
                                                                        @endforeach
                                                                    @else
                                                                        -
                                                                    @endif</td>
                                                                    @if($invoice->discount_apply==1)
                                                               <td>{{($item->discount!=0)?\App\Models\Utility::priceFormat($settings,$item->discount):'-'}}</td>
                                                            @else
                                                                <td></td>
                                                            @endif
                                                        <td>{{!empty($item->description)?$item->description:'-'}}</td>
                                                        <td>{{\App\Models\Utility::priceFormat($settings,$item->price * $item->quantity)}}</td>
                                                       
                                                        
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                   
                                                        <tr>
                                                            <td>{{__('Total')}}</td>
                                                            <td>{{$invoice->totalQuantity}}</td>
                                                            <td>{{\App\Models\Utility::priceFormat($settings,$invoice->totalRate)}}</td>
                                                            <td>{{\App\Models\Utility::priceFormat($settings,$invoice->totalTaxPrice) }}</td>
                                                            @if($invoice->discount_apply==1)
                                                            <td>{{\App\Models\Utility::priceFormat($settings,$invoice->totalDiscount)}}</td>
                                                            @else
                                                            <td></td>
                                                            @endif
                                                            <td></td>
                                                            <td>{{\App\Models\Utility::priceFormat($settings,$invoice->getSubTotal())}}</td>
                                                </tr>
                                                 </tbody>
                                                 <tfoot>
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{('Subtotal')}}</td><td>{{\App\Models\Utility::priceFormat($settings,$invoice->getSubTotal())}}</td></tr>
                                                     @if($invoice->getTotalDiscount())
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{('Discount')}}</td><td>{{\App\Models\Utility::priceFormat($settings,$invoice->getTotalDiscount())}}</td></tr>
                                                     @endif
                                                     @if(!empty($invoice->taxesData))
                                                        @foreach($invoice->taxesData as $taxName => $taxPrice)
                                                          
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{$taxName}}</td><td>{{ \App\Models\Utility::priceFormat($settings,$taxPrice)  }}</td></tr>
                                                     @endforeach
                                                    @endif
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{__('Total')}}</td><td>{{\App\Models\Utility::priceFormat($settings,$invoice->getSubTotal()-$invoice->getTotalDiscount()+$invoice->getTotalTax())}}</td></tr>
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{__('Paid')}}</td><td>{{\App\Models\Utility::priceFormat($settings,($invoice->getTotal()-$invoice->getDue())-($invoice->invoiceTotalCreditNote()))}}</td></tr>
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{__('Credit Note')}}</td><td>{{\App\Models\Utility::priceFormat($settings,($invoice->invoiceTotalCreditNote()))}}</td></tr>
                                                     <tr><td colspan="6" style="text-align:right;font-weight: bold;">{{__('Due Amount')}}</td><td>{{\App\Models\Utility::priceFormat($settings,$invoice->getDue())}}</td></tr>
                                                     
                                                 </tfoot>
                                        </table>
                                            


                                            <div data-v-f2a183a6="" class="d-header-50">
                                                <p data-v-f2a183a6="">
                                                    {{$settings['footer_title']}} <br>
                                                    {{$settings['footer_notes']}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!isset($preview))
    @include('invoice.script');
@endif
</body>
</html>
