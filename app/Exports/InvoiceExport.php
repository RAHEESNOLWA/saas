<?php

namespace App\Exports;

use App\Models\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoiceExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = Invoice::get();

        foreach ($data as $k => $Invoice) {
            unset($Invoice->created_by, $Invoice->updated_at, $Invoice->created_at);
            // $data[$k]["Invoice_id"] = \Auth::user()->InvoiceNumberFormat($Invoice->Invoice_id);
            // $data[$k]["category_id"] = \Auth::user()->InvoiceNumberFormat($Invoice->category_id);

        }

        return $data;
    }

    public function headings(): array
    {
        return [
            "id",
            "Invoice Id",
            "Customer Id",
            "Issue Date",
            "Due Date",
            "Send Date",
            "Category Id",
            "Ref number",
            "status",
            "Shipping Display",
            "discount_apply",
        ];
    }
}
