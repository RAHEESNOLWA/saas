<?php

namespace App\Exports;

use App\Models\ProductService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductServiceExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        $data = ProductService::get();

        foreach ($data as $k => $ProductService) {
            unset($ProductService->created_by,$ProductService->sku, $ProductService->updated_at, $ProductService->created_at);
            // $data[$k]["ProductService_id"] = \Auth::user()->ProductServiceNumberFormat($ProductService->ProductService_id);
            // $data[$k]["category_id"] = \Auth::user()->ProductServiceNumberFormat($ProductService->category_id);

        }

        return $data;
    }

    public function headings(): array
    {
        return [
            "ID",
            "Name",
            "sale_price",
            "purchase_price",
            "Quantity",
            "tax_id",
            "category_id",
            "Unit",
            "Type",
            "Description",
        ];
    }
}
