<?php

namespace App\Exports;

use App\Models\Bill;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BillExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = Bill::get();

        foreach ($data as $k => $Bill) {
            unset($Bill->created_by, $Bill->updated_at, $Bill->created_at);
            // $data[$k]["Bill_id"] = \Auth::user()->BillNumberFormat($Bill->Bill_id);
            // $data[$k]["category_id"] = \Auth::user()->BillNumberFormat($Bill->category_id);

        }

        return $data;
    }

    public function headings(): array
    {
        return [
            "Id",
            "Bill Id",
            "vender_id",
            "bill_date",
            "due_date",
            "order_number",
            "status",
            "shipping_display",
            "send_date",
            "discount_apply",
            "category_id",

        ];
    }
}
