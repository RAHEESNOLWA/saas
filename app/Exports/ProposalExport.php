<?php

namespace App\Exports;

use App\Models\Proposal;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProposalExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = Proposal::get();

        foreach ($data as $k => $Proposal) {
            unset($Proposal->created_by, $Proposal->updated_at, $Proposal->created_at);
            // $data[$k]["Proposal_id"] = \Auth::user()->ProposalNumberFormat($Proposal->Proposal_id);
            // $data[$k]["category_id"] = \Auth::user()->ProposalNumberFormat($Proposal->category_id);

        }

        return $data;
    }

    public function headings(): array
    {
        return [
            "id",
            "Proposal Id",
            "Customer Id",
            "issue Date",
            "Send Date",
            "Category Id",
            "status",
            "discount_apply",
            "converted_invoice_id",
            "is_convert",
        ];
    }
}
