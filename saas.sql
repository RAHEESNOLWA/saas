-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2022 at 07:01 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saas`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_payment_settings`
--

CREATE TABLE `admin_payment_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_date` date NOT NULL,
  `supported_date` date NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bank_accounts`
--

CREATE TABLE `bank_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `holder_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` double(15,2) NOT NULL DEFAULT 0.00,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank_accounts`
--

INSERT INTO `bank_accounts` (`id`, `holder_name`, `bank_name`, `account_number`, `opening_balance`, `contact_number`, `bank_address`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Cash', '', '-', 0.00, '-', '-', 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `vender_id` int(11) NOT NULL,
  `bill_date` date NOT NULL,
  `due_date` date NOT NULL,
  `order_number` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `shipping_display` int(11) NOT NULL DEFAULT 1,
  `send_date` date DEFAULT NULL,
  `discount_apply` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_payments`
--

CREATE TABLE `bill_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `account_id` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_products`
--

CREATE TABLE `bill_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `tax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0.00',
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chart_of_accounts`
--

CREATE TABLE `chart_of_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 0,
  `sub_type` int(11) NOT NULL DEFAULT 0,
  `is_enabled` int(11) NOT NULL DEFAULT 1,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chart_of_accounts`
--

INSERT INTO `chart_of_accounts` (`id`, `name`, `code`, `type`, `sub_type`, `is_enabled`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Accounts Receivable', 120, 1, 1, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(2, 'Computer Equipment', 160, 1, 2, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(3, 'Office Equipment', 150, 1, 2, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(4, 'Inventory', 140, 1, 3, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(5, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(6, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(7, 'Accounts Payable', 200, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(8, 'Accruals', 205, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(9, 'Office Equipment', 150, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(10, 'Clearing Account', 855, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(11, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(12, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(13, 'Historical Adjustments', 255, 2, 8, 1, NULL, 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(14, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(15, 'Rounding', 260, 2, 8, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(16, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(17, 'Advertising', 600, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(18, 'Automobile Expenses', 644, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(19, 'Bad Debts', 684, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(20, 'Bank Revaluations', 810, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(21, 'Bank Service Charges', 605, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(22, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(23, 'Depreciation', 700, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(24, 'General Expenses', 628, 3, 12, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(25, 'Interest Income', 460, 4, 13, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(26, 'Other Revenue', 470, 4, 13, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(27, 'Purchase Discount', 475, 4, 13, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(28, 'Sales', 400, 4, 13, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(29, 'Common Stock', 330, 5, 16, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(30, 'Owners Contribution', 300, 5, 16, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(31, 'Owners Draw', 310, 5, 16, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(32, 'Retained Earnings', 320, 5, 16, 1, NULL, 2, '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(33, 'Accounts Receivable', 120, 1, 1, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(34, 'Computer Equipment', 160, 1, 2, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(35, 'Office Equipment', 150, 1, 2, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(36, 'Inventory', 140, 1, 3, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(37, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(38, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(39, 'Accounts Payable', 200, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(40, 'Accruals', 205, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(41, 'Office Equipment', 150, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(42, 'Clearing Account', 855, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(43, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(44, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(45, 'Historical Adjustments', 255, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(46, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(47, 'Rounding', 260, 2, 8, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(48, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(49, 'Advertising', 600, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(50, 'Automobile Expenses', 644, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(51, 'Bad Debts', 684, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(52, 'Bank Revaluations', 810, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(53, 'Bank Service Charges', 605, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(54, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(55, 'Depreciation', 700, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(56, 'General Expenses', 628, 3, 12, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(57, 'Interest Income', 460, 4, 13, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(58, 'Other Revenue', 470, 4, 13, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(59, 'Purchase Discount', 475, 4, 13, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(60, 'Sales', 400, 4, 13, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(61, 'Common Stock', 330, 5, 16, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(62, 'Owners Contribution', 300, 5, 16, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(63, 'Owners Draw', 310, 5, 16, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(64, 'Retained Earnings', 320, 5, 16, 1, NULL, 4, '2021-12-21 03:09:39', '2021-12-21 03:09:39'),
(65, 'Accounts Receivable', 120, 1, 1, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(66, 'Computer Equipment', 160, 1, 2, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(67, 'Office Equipment', 150, 1, 2, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(68, 'Inventory', 140, 1, 3, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(69, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(70, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(71, 'Accounts Payable', 200, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(72, 'Accruals', 205, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(73, 'Office Equipment', 150, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(74, 'Clearing Account', 855, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(75, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(76, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 5, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(77, 'Historical Adjustments', 255, 2, 8, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(78, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(79, 'Rounding', 260, 2, 8, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(80, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(81, 'Advertising', 600, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(82, 'Automobile Expenses', 644, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(83, 'Bad Debts', 684, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(84, 'Bank Revaluations', 810, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(85, 'Bank Service Charges', 605, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(86, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(87, 'Depreciation', 700, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(88, 'General Expenses', 628, 3, 12, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(89, 'Interest Income', 460, 4, 13, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(90, 'Other Revenue', 470, 4, 13, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(91, 'Purchase Discount', 475, 4, 13, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(92, 'Sales', 400, 4, 13, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(93, 'Common Stock', 330, 5, 16, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(94, 'Owners Contribution', 300, 5, 16, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(95, 'Owners Draw', 310, 5, 16, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(96, 'Retained Earnings', 320, 5, 16, 1, NULL, 5, '2021-12-24 03:38:06', '2021-12-24 03:38:06'),
(97, 'Accounts Receivable', 120, 1, 1, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(98, 'Computer Equipment', 160, 1, 2, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(99, 'Office Equipment', 150, 1, 2, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(100, 'Inventory', 140, 1, 3, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(101, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(102, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(103, 'Accounts Payable', 200, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(104, 'Accruals', 205, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(105, 'Office Equipment', 150, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(106, 'Clearing Account', 855, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(107, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(108, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(109, 'Historical Adjustments', 255, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(110, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(111, 'Rounding', 260, 2, 8, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(112, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(113, 'Advertising', 600, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(114, 'Automobile Expenses', 644, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(115, 'Bad Debts', 684, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(116, 'Bank Revaluations', 810, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(117, 'Bank Service Charges', 605, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(118, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(119, 'Depreciation', 700, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(120, 'General Expenses', 628, 3, 12, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(121, 'Interest Income', 460, 4, 13, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(122, 'Other Revenue', 470, 4, 13, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(123, 'Purchase Discount', 475, 4, 13, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(124, 'Sales', 400, 4, 13, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(125, 'Common Stock', 330, 5, 16, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(126, 'Owners Contribution', 300, 5, 16, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(127, 'Owners Draw', 310, 5, 16, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(128, 'Retained Earnings', 320, 5, 16, 1, NULL, 6, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(129, 'Accounts Receivable', 120, 1, 1, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(130, 'Computer Equipment', 160, 1, 2, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(131, 'Office Equipment', 150, 1, 2, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(132, 'Inventory', 140, 1, 3, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(133, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(134, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(135, 'Accounts Payable', 200, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(136, 'Accruals', 205, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(137, 'Office Equipment', 150, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(138, 'Clearing Account', 855, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(139, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(140, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(141, 'Historical Adjustments', 255, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(142, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(143, 'Rounding', 260, 2, 8, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(144, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(145, 'Advertising', 600, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(146, 'Automobile Expenses', 644, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(147, 'Bad Debts', 684, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(148, 'Bank Revaluations', 810, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(149, 'Bank Service Charges', 605, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(150, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(151, 'Depreciation', 700, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(152, 'General Expenses', 628, 3, 12, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(153, 'Interest Income', 460, 4, 13, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(154, 'Other Revenue', 470, 4, 13, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(155, 'Purchase Discount', 475, 4, 13, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(156, 'Sales', 400, 4, 13, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(157, 'Common Stock', 330, 5, 16, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(158, 'Owners Contribution', 300, 5, 16, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(159, 'Owners Draw', 310, 5, 16, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(160, 'Retained Earnings', 320, 5, 16, 1, NULL, 7, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(161, 'Accounts Receivable', 120, 1, 1, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(162, 'Computer Equipment', 160, 1, 2, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(163, 'Office Equipment', 150, 1, 2, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(164, 'Inventory', 140, 1, 3, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(165, 'Budget - Finance Staff', 857, 1, 6, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(166, 'Accumulated Depreciation', 170, 1, 7, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(167, 'Accounts Payable', 200, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(168, 'Accruals', 205, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(169, 'Office Equipment', 150, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(170, 'Clearing Account', 855, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(171, 'Employee Benefits Payable', 235, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(172, 'Employee Deductions payable', 236, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(173, 'Historical Adjustments', 255, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(174, 'Revenue Received in Advance', 835, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(175, 'Rounding', 260, 2, 8, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(176, 'Costs of Goods Sold', 500, 3, 11, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(177, 'Advertising', 600, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(178, 'Automobile Expenses', 644, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(179, 'Bad Debts', 684, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(180, 'Bank Revaluations', 810, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(181, 'Bank Service Charges', 605, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(182, 'Consulting & Accounting', 615, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(183, 'Depreciation', 700, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(184, 'General Expenses', 628, 3, 12, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(185, 'Interest Income', 460, 4, 13, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(186, 'Other Revenue', 470, 4, 13, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(187, 'Purchase Discount', 475, 4, 13, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(188, 'Sales', 400, 4, 13, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(189, 'Common Stock', 330, 5, 16, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(190, 'Owners Contribution', 300, 5, 16, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(191, 'Owners Draw', 310, 5, 16, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(192, 'Retained Earnings', 320, 5, 16, 1, NULL, 8, '2021-12-27 00:23:48', '2021-12-27 00:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `chart_of_account_sub_types`
--

CREATE TABLE `chart_of_account_sub_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `type` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chart_of_account_sub_types`
--

INSERT INTO `chart_of_account_sub_types` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Current Asset', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(2, 'Fixed Asset', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(3, 'Inventory', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(4, 'Non-current Asset', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(5, 'Prepayment', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(6, 'Bank & Cash', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(7, 'Depreciation', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(8, 'Current Liability', 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(9, 'Liability', 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(10, 'Non-current Liability', 2, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(11, 'Direct Costs', 3, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(12, 'Expense', 3, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(13, 'Revenue', 4, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(14, 'Sales', 4, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(15, 'Other Income', 4, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(16, 'Equity', 5, '2021-12-20 05:32:43', '2021-12-20 05:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `chart_of_account_types`
--

CREATE TABLE `chart_of_account_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chart_of_account_types`
--

INSERT INTO `chart_of_account_types` (`id`, `name`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Assets', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(2, 'Liabilities', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(3, 'Expenses', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(4, 'Income', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(5, 'Equity', 1, '2021-12-20 05:32:43', '2021-12-20 05:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `company_payment_settings`
--

CREATE TABLE `company_payment_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `limit` int(11) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `credit_notes`
--

CREATE TABLE `credit_notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice` int(11) NOT NULL DEFAULT 0,
  `customer` int(11) NOT NULL DEFAULT 0,
  `amount` double(15,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(11) NOT NULL DEFAULT 0,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `billing_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_name_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_city_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_other` text CHARACTER SET utf8 NOT NULL,
  `shipping_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_name_other` text CHARACTER SET utf8 NOT NULL,
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country_other` text CHARACTER SET utf8 NOT NULL,
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_other` text CHARACTER SET utf8 NOT NULL,
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city_other` text CHARACTER SET utf8 NOT NULL,
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_other` text CHARACTER SET utf8 NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `balance` double(8,2) NOT NULL DEFAULT 0.00,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_id`, `name`, `name_other`, `email`, `password`, `contact`, `avatar`, `created_by`, `is_active`, `email_verified_at`, `billing_name`, `billing_name_other`, `billing_country`, `billing_country_other`, `billing_state`, `billing_state_other`, `billing_city`, `billing_city_other`, `billing_phone`, `billing_zip`, `billing_address`, `billing_address_other`, `shipping_name`, `shipping_name_other`, `shipping_country`, `shipping_country_other`, `shipping_state`, `shipping_state_other`, `shipping_city`, `shipping_city_other`, `shipping_phone`, `shipping_zip`, `shipping_address`, `shipping_address_other`, `lang`, `balance`, `remember_token`, `last_login_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rose', '', 'rose@gmail.com', '$2y$10$4e1rhKRn2dFjHYwQmJEbFuR6hQ32P2Q6dJ9JLXE8JirL8POZRc99G', '8281066602', '', 4, 1, NULL, 'rose', '', 'india', '', 'kerala', '', 'calicut', '', '8281066602', '673620', 'rose villa', '', 'rose', '', 'india', '', 'kerala', '', 'calicut', '', '8281066602', '321321', 'werwrwerwe', '', '', 217.00, NULL, NULL, '2021-12-21 03:21:26', '2021-12-29 23:05:57'),
(2, 2, 'zxzsf', '', 'abc@g.c', '$2y$10$ML1waVli9sgy.VpdS0QUWOdHYbUeTj3SQLre5J3iUqqTcdyS9h9qe', '32424234', '', 4, 1, NULL, 'dfgdfg', '', 'sdfsdf', '', 'sdfsdf', '', 'sdff', '', '11233213', '123123', 'fsdfsdf', '', 'dfgdfg', '', 'sdfsdf', '', 'sdfsdf', '', 'sdff', '', '11233213', '123123', 'fsdfsdf', '', '', 0.00, NULL, NULL, '2021-12-22 23:39:31', '2021-12-22 23:39:31'),
(3, 3, 'dfgdfg', 'كوزيكود', 'abc@df.c', '$2y$10$wXQBIrwFHgUgJvSf46M6C./6AOchVZbBBjoz1smF5r0brYssek.Ri', '3424234', '', 4, 1, NULL, 'fsdf', 'كوزيكود', 'sdfdsf', 'كوزيكود', 'fsdf', 'كوزيكود', 'fsdf', 'كوزيكود', '123213', '123123', 'fsdfsdffdgdfg', 'كوزيكود', 'fsdf', 'كوزيكود', 'sdfdsf', 'كوزيكود', 'fsdf', 'كوزيكود', 'fsdf', 'كوزيكود', '123213', '123123', 'fsdfsdffdgdfg', 'كوزيكود', '', 0.00, NULL, NULL, '2021-12-22 23:45:34', '2021-12-22 23:45:34'),
(4, 4, 'athul', 'أثول', 'athul@gmail.com', '$2y$10$sBLoQFnQwIWseVtW67KKXeIi28iIGcTNFm2C41EYRSFYgG/Myku/W', '9656671088', '', 4, 1, NULL, 'athul', 'أثول', 'India', 'الهند', 'kerala', 'ولاية كيرالا', 'calicut', 'كاليكوت', '08281066602', '673620', 'vallath', 'فالاث', 'athul', 'أثول', 'India', 'الهند', 'kerala', 'ولاية كيرالا', 'calicut', 'كاليكوت', '08281066602', '673620', 'vallath', 'فالاث', '', 465.30, NULL, NULL, '2021-12-23 01:25:26', '2021-12-24 02:19:38'),
(5, 5, 'nolwa', 'dsfsdf', 'harsha.4harsha@gmail.com', '$2y$10$QRWh4sRXuoTlEAOlaTjEK.fqv41tNYRheGDxrePzEp2s145VVpXky', '1231313', '', 4, 1, NULL, 'harsha', 'sadasd', 'India', 'كوزيكود', 'kerala', 'كوزيكود', 'calicut', 'كاليكوت', '08281066602', '673620', 'harsha villa', 'fdgdfgdfg', 'athul', 'fdgdfg', 'India', 'dfgdfgfd', 'kerala', 'gdfgdfg', 'calicut', 'dfgdfg', '08281066602', '673620', 'athul  villa', 'dsdaasd', '', 238.30, NULL, NULL, '2022-01-19 02:18:37', '2022-01-19 02:20:05');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `record_id` bigint(20) UNSIGNED NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `debit_notes`
--

CREATE TABLE `debit_notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill` int(11) NOT NULL DEFAULT 0,
  `vendor` int(11) NOT NULL DEFAULT 0,
  `amount` double(15,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goals`
--

CREATE TABLE `goals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `is_display` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `issue_date` date NOT NULL,
  `due_date` date NOT NULL,
  `send_date` date DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `ref_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `shipping_display` int(11) NOT NULL DEFAULT 1,
  `discount_apply` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_id`, `customer_id`, `issue_date`, `due_date`, `send_date`, `category_id`, `ref_number`, `status`, `shipping_display`, `discount_apply`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-12-21', '2021-12-21', '2021-12-24', 3, '98798', 1, 1, 0, 4, '2021-12-21 04:04:26', '2021-12-24 04:13:07'),
(3, 2, 4, '2021-12-23', '2021-12-23', '2021-12-24', 3, '789', 1, 1, 0, 4, '2021-12-23 01:26:17', '2021-12-24 02:19:38'),
(4, 3, 1, '2021-12-28', '2021-12-28', '2021-12-30', 3, '21321', 1, 1, 1, 4, '2021-12-28 05:53:27', '2021-12-29 23:05:57'),
(5, 4, 5, '2022-01-19', '2022-01-19', '2022-01-19', 3, '131312323', 1, 1, 0, 4, '2022-01-19 02:19:49', '2022-01-19 02:20:05');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `account_id` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Manually',
  `txn_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_products`
--

CREATE TABLE `invoice_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `tax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0.00',
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_products`
--

INSERT INTO `invoice_products` (`id`, `invoice_id`, `product_id`, `quantity`, `tax`, `discount`, `price`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, '1', 1.00, 100.00, 'test', '2021-12-21 04:04:26', '2021-12-21 04:04:26'),
(3, 3, 1, 3, '1', 0.00, 100.00, 'fgdfgfdg', '2021-12-23 01:26:17', '2021-12-23 01:26:17'),
(4, 3, 3, 1, '1', 0.00, 123.00, 'dfgfdg', '2021-12-23 01:26:17', '2021-12-23 01:26:17'),
(5, 4, 1, 1, '1', 1.00, 100.00, '', '2021-12-28 05:53:27', '2021-12-28 05:53:27'),
(6, 4, 2, 1, '1', 2.00, 100.00, '', '2021-12-28 05:53:27', '2021-12-28 05:53:27'),
(7, 5, 1, 1, '1', 5.00, 100.00, 'dfgfg', '2022-01-19 02:19:49', '2022-01-19 02:19:49'),
(8, 5, 3, 1, '1', 2.00, 123.00, '', '2022-01-19 02:19:49', '2022-01-19 02:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `journal_entries`
--

CREATE TABLE `journal_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `journal_id` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `journal_items`
--

CREATE TABLE `journal_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `journal` int(11) NOT NULL DEFAULT 0,
  `account` int(11) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit` double(8,2) NOT NULL DEFAULT 0.00,
  `credit` double(8,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `landing_page_sections`
--

CREATE TABLE `landing_page_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_order` int(11) NOT NULL DEFAULT 0,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_type` enum('section-1','section-2','section-3','section-4','section-5','section-6','section-7','section-8','section-9','section-10','section-plan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_demo_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_blade_file_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landing_page_sections`
--

INSERT INTO `landing_page_sections` (`id`, `section_name`, `section_order`, `content`, `section_type`, `default_content`, `section_demo_image`, `section_blade_file_name`, `created_at`, `updated_at`) VALUES
(1, 'section-1', 1, '{\"logo\":\"landing_logo.png\",\"image\":\"top-banner.png\",\"button\":{\"text\":\"Login\"},\"menu\":[{\"menu\":\"Features\",\"href\":\"#\"},{\"menu\":\"Pricing\",\"href\":\"#\"}],\"text\":{\"text-1\":\"AccountGo Saas\",\"text-2\":\"Accounting and Billing Tool\",\"text-3\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\",\"text-4\":\"get started - its free\",\"text-5\":\"no creadit card reqired\"},\"custom_class_name\":\"\"}', 'section-1', '{\"logo\":\"landing_logo.png\",\"image\":\"top-banner.png\",\"button\":{\"text\":\"Login\"},\"menu\":[{\"menu\":\"Features\",\"href\":\"#\"},{\"menu\":\"Pricing\",\"href\":\"#\"}],\"text\":{\"text-1\":\"AccountGo Saas\",\"text-2\":\"Accounting and Billing Tool\",\"text-3\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\",\"text-4\":\"get started - its free\",\"text-5\":\"no creadit card reqired \"},\"custom_class_name\":\"\"}', 'top-header-section.png', 'custome-top-header-section', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(2, 'section-2', 2, '{\"image\":\"cal-sec.png\",\"button\":{\"text\":\"try our system\",\"href\":\"#\"},\"text\":{\"text-1\":\"Features\",\"text-2\":\"Lorem Ipsum is simply dummy\",\"text-3\":\"text of the printing\",\"text-4\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting\"},\"image_array\":[{\"id\":1,\"image\":\"nexo.png\"},{\"id\":2,\"image\":\"edge.png\"},{\"id\":3,\"image\":\"atomic.png\"},{\"id\":4,\"image\":\"brd.png\"},{\"id\":5,\"image\":\"trust.png\"},{\"id\":6,\"image\":\"keep-key.png\"},{\"id\":7,\"image\":\"atomic.png\"},{\"id\":8,\"image\":\"edge.png\"}],\"custom_class_name\":\"\"}', 'section-2', '{\"image\":\"cal-sec.png\",\"button\":{\"text\":\"try our system\",\"href\":\"#\"},\"text\":{\"text-1\":\"Features\",\"text-2\":\"Lorem Ipsum is simply dummy\",\"text-3\":\"text of the printing\",\"text-4\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting\"},\"image_array\":[{\"id\":1,\"image\":\"nexo.png\"},{\"id\":2,\"image\":\"edge.png\"},{\"id\":3,\"image\":\"atomic.png\"},{\"id\":4,\"image\":\"brd.png\"},{\"id\":5,\"image\":\"trust.png\"},{\"id\":6,\"image\":\"keep-key.png\"},{\"id\":7,\"image\":\"atomic.png\"},{\"id\":8,\"image\":\"edge.png\"}],\"custom_class_name\":\"\"}', 'logo-part-main-back-part.png', 'custome-logo-part-main-back-part', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(3, 'section-3', 3, NULL, 'section-3', '{\"image\": \"sec-2.png\",\"button\": {\"text\": \"try our system\",\"href\": \"#\"},\"text\": {\"text-1\": \"Features\",\"text-2\": \"Lorem Ipsum is simply dummy\",\"text-3\": \"text of the printing\",\"text-4\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting\"},\"custom_class_name\":\"\"}', 'simple-sec-even.png', 'custome-simple-sec-even', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(4, 'section-4', 4, NULL, 'section-4', '{\"image\": \"sec-3.png\",\"button\": {\"text\": \"try our system\",\"href\": \"#\"},\"text\": {\"text-1\": \"Features\",\"text-2\": \"Lorem Ipsum is simply dummy\",\"text-3\": \"text of the printing\",\"text-4\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting\"},\"custom_class_name\":\"\"}', 'simple-sec-odd.png', 'custome-simple-sec-odd', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(5, 'section-5', 5, NULL, 'section-5', '{\"button\": {\"text\": \"TRY OUR SYSTEM\",\"href\": \"#\"},\"text\": {\"text-1\": \"See more features\",\"text-2\": \"All Features\",\"text-3\": \"in one place\",\"text-4\": \"Attractive Dashboard Customer & Vendor Login Multi Languages\",\"text-5\":\"Invoice, Billing & Transaction Multi User & Permission Paypal & Stripe for Invoice User Friendly Invoice Theme Make your own setting\",\"text-6\":\"Multi User & Permission Paypal & Stripe for Invoice User Friendly Invoice Theme Make your own setting\",\"text-7\":\"Multi User & Permission Paypal & Stripe for Invoice User Friendly Invoice Theme Make your own setting User Friendly Invoice Theme Make your own setting\",\"text-8\":\"Multi User & Permission Paypal & Stripe for Invoice\"},\"custom_class_name\":\"\"}', 'features-inner-part.png', 'custome-features-inner-part', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(6, 'section-6', 6, '{\"system\":[{\"id\":1,\"name\":\"Dashboard\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"},{\"data_id\":4,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":5,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":2,\"name\":\"Functions\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"}]},{\"id\":3,\"name\":\"Reports\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":4,\"name\":\"Tables\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"},{\"data_id\":4,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"}]},{\"id\":5,\"name\":\"Settings\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":6,\"name\":\"Contact\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"}]}],\"custom_class_name\":\"\"}', 'section-6', '{\"system\":[{\"id\":1,\"name\":\"Dashboard\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"},{\"data_id\":4,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":5,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":2,\"name\":\"Functions\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"}]},{\"id\":3,\"name\":\"Reports\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":4,\"name\":\"Tables\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"},{\"data_id\":3,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-3.png\"},{\"data_id\":4,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"}]},{\"id\":5,\"name\":\"Settings\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"},{\"data_id\":2,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-2.png\"}]},{\"id\":6,\"name\":\"Contact\",\"data\":[{\"data_id\":1,\"text\":{\"text_1\":\"Dashboard\",\"text_2\":\"Main Page\"},\"button\":{\"text\":\"LIVE DEMO\",\"href\":\"#\"},\"image\":\"tab-1.png\"}]}],\"custom_class_name\":\"\"}', 'container-our-system-div.png', 'custome-container-our-system-div', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(7, 'section-7', 7, NULL, 'section-7', '{\"testimonials\":[{\"id\":1,\"text\":{\"text_1\":\"We have been building AI projects for a long time and we decided it was time to build a platform that can streamline the broken process that we had to put up with. Here are some of the key things we wish we had when we were building projects before.\",\"text_2\":\"Lorem Ipsum\",\"text_3\":\"Founder and CEO at Rajodiya Infotech\"},\"image\":\"testimonials-img.png\"},{\"id\":2,\"text\":{\"text_1\":\"We have been building AI projects for a long time and we decided it was time to build a platform that can streamline the broken process that we had to put up with. Here are some of the key things we wish we had when we were building projects before.\",\"text_2\":\"Lorem Ipsum\",\"text_3\":\"Founder and CEO at Rajodiya Infotech\"},\"image\":\"testimonials-img.png\"},{\"id\":3,\"text\":{\"text_1\":\"We have been building AI projects for a long time and we decided it was time to build a platform that can streamline the broken process that we had to put up with. Here are some of the key things we wish we had when we were building projects before.\",\"text_2\":\"Lorem Ipsum\",\"text_3\":\"Founder and CEO at Rajodiya Infotech\"},\"image\":\"testimonials-img.png\"},{\"id\":4,\"text\":{\"text_1\":\"We have been building AI projects for a long time and we decided it was time to build a platform that can streamline the broken process that we had to put up with. Here are some of the key things we wish we had when we were building projects before.\",\"text_2\":\"Lorem Ipsum\",\"text_3\":\"Founder and CEO at Rajodiya Infotech\"},\"image\":\"testimonials-img.png\"},{\"id\":5,\"text\":{\"text_1\":\"We have been building AI projects for a long time and we decided it was time to build a platform that can streamline the broken process that we had to put up with. Here are some of the key things we wish we had when we were building projects before.\",\"text_2\":\"Lorem Ipsum\",\"text_3\":\"Founder and CEO at Rajodiya Infotech\"},\"image\":\"testimonials-img.png\"}],\"custom_class_name\":\"\"}', 'testimonials-section.png', 'custome-testimonials-section', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(8, 'section-plan', 8, 'plan', 'section-plan', 'plan', 'plan-section.png', 'plan-section', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(9, 'section-8', 9, '{\"button\":{\"text\":\"Subscribe\"},\"text\":{\"text-1\":\"Try for free\",\"text-2\":\"Lorem Ipsum is simply dummy text\",\"text-3\":\"of the printing and typesetting industry\",\"text-4\":\"Type your email address and click the button\"},\"custom_class_name\":\"\"}', 'section-8', '{\"button\":{\"text\":\"Subscribe\"},\"text\":{\"text-1\":\"Try for free\",\"text-2\":\"Lorem Ipsum is simply dummy text\",\"text-3\":\"of the printing and typesetting industry\",\"text-4\":\"Type your email address and click the button\"},\"custom_class_name\":\"\"}', 'subscribe-part.png', 'custome-subscribe-part', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(10, 'section-9', 10, '{\"menu\":[{\"menu\":\"Facebook\",\"href\":\"#\"},{\"menu\":\"LinkedIn\",\"href\":\"#\"},{\"menu\":\"Twitter\",\"href\":\"#\"},{\"menu\":\"Discord\",\"href\":\"#\"}],\"custom_class_name\":\"\"}', 'section-9', '{\"menu\":[{\"menu\":\"Facebook\",\"href\":\"#\"},{\"menu\":\"LinkedIn\",\"href\":\"#\"},{\"menu\":\"Twitter\",\"href\":\"#\"},{\"menu\":\"Discord\",\"href\":\"#\"}],\"custom_class_name\":\"\"}', 'social-links.png', 'custome-social-links', '2021-12-20 05:32:44', '2021-12-20 05:32:44'),
(11, 'section-10', 11, '{\"footer\":{\"logo\":{\"logo\":\"landing_logo.png\"},\"footer_menu\":[{\"id\":1,\"menu\":\"FIO Protocol\",\"data\":[{\"menu_name\":\"Feature\",\"menu_href\":\"#\"},{\"menu_name\":\"Download\",\"menu_href\":\"#\"},{\"menu_name\":\"Integration\",\"menu_href\":\"#\"},{\"menu_name\":\"Extras\",\"menu_href\":\"#\"}]},{\"id\":2,\"menu\":\"Learn\",\"data\":[{\"menu_name\":\"Feature\",\"menu_href\":\"#\"},{\"menu_name\":\"Download\",\"menu_href\":\"#\"},{\"menu_name\":\"Integration\",\"menu_href\":\"#\"},{\"menu_name\":\"Extras\",\"menu_href\":\"#\"}]},{\"id\":3,\"menu\":\"Foundation\",\"data\":[{\"menu_name\":\"About Us\",\"menu_href\":\"#\"},{\"menu_name\":\"Customers\",\"menu_href\":\"#\"},{\"menu_name\":\"Resources\",\"menu_href\":\"#\"},{\"menu_name\":\"Blog\",\"menu_href\":\"#\"}]}],\"contact_app\":[{\"menu\":\"Contact\",\"data\":[{\"id\":1,\"image\":\"app-store.png\",\"image_href\":\"#\"},{\"id\":2,\"image\":\"google-pay.png\",\"image_href\":\"#\"}]}],\"bottom_menu\":{\"text\":\"All rights reserved.\",\"data\":[{\"menu_name\":\"Privacy Policy\",\"menu_href\":\"#\"},{\"menu_name\":\"Github\",\"menu_href\":\"#\"},{\"menu_name\":\"Press Kit\",\"menu_href\":\"#\"},{\"menu_name\":\"Contact\",\"menu_href\":\"#\"}]}},\"custom_class_name\":\"\"}', 'section-10', '{\"footer\":{\"logo\":{\"logo\":\"landing_logo.png\"},\"footer_menu\":[{\"id\":1,\"menu\":\"FIO Protocol\",\"data\":[{\"menu_name\":\"Feature\",\"menu_href\":\"#\"},{\"menu_name\":\"Download\",\"menu_href\":\"#\"},{\"menu_name\":\"Integration\",\"menu_href\":\"#\"},{\"menu_name\":\"Extras\",\"menu_href\":\"#\"}]},{\"id\":2,\"menu\":\"Learn\",\"data\":[{\"menu_name\":\"Feature\",\"menu_href\":\"#\"},{\"menu_name\":\"Download\",\"menu_href\":\"#\"},{\"menu_name\":\"Integration\",\"menu_href\":\"#\"},{\"menu_name\":\"Extras\",\"menu_href\":\"#\"}]},{\"id\":3,\"menu\":\"Foundation\",\"data\":[{\"menu_name\":\"About Us\",\"menu_href\":\"#\"},{\"menu_name\":\"Customers\",\"menu_href\":\"#\"},{\"menu_name\":\"Resources\",\"menu_href\":\"#\"},{\"menu_name\":\"Blog\",\"menu_href\":\"#\"}]}],\"contact_app\":[{\"menu\":\"Contact\",\"data\":[{\"id\":1,\"image\":\"app-store.png\",\"image_href\":\"#\"},{\"id\":2,\"image\":\"google-pay.png\",\"image_href\":\"#\"}]}],\"bottom_menu\":{\"text\":\"All rights reserved.\",\"data\":[{\"menu_name\":\"Privacy Policy\",\"menu_href\":\"#\"},{\"menu_name\":\"Github\",\"menu_href\":\"#\"},{\"menu_name\":\"Press Kit\",\"menu_href\":\"#\"},{\"menu_name\":\"Contact\",\"menu_href\":\"#\"}]}},\"custom_class_name\":\"\"}', 'footer-section.png', 'custome-footer-section', '2021-12-20 05:32:44', '2021-12-20 05:32:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_09_28_102009_create_settings_table', 1),
(5, '2019_11_13_051828_create_taxes_table', 1),
(6, '2019_11_13_055026_create_invoices_table', 1),
(7, '2019_11_13_072623_create_expenses_table', 1),
(8, '2019_11_21_090403_create_plans_table', 1),
(9, '2020_01_08_063207_create_product_services_table', 1),
(10, '2020_01_08_084029_create_product_service_categories_table', 1),
(11, '2020_01_08_092717_create_product_service_units_table', 1),
(12, '2020_01_08_121541_create_customers_table', 1),
(13, '2020_01_09_104945_create_venders_table', 1),
(14, '2020_01_09_113852_create_bank_accounts_table', 1),
(15, '2020_01_09_124222_create_transfers_table', 1),
(16, '2020_01_10_064723_create_transactions_table', 1),
(17, '2020_01_13_072608_create_invoice_products_table', 1),
(18, '2020_01_15_034438_create_revenues_table', 1),
(19, '2020_01_15_051228_create_bills_table', 1),
(20, '2020_01_15_060859_create_bill_products_table', 1),
(21, '2020_01_15_073237_create_payments_table', 1),
(22, '2020_01_16_043907_create_orders_table', 1),
(23, '2020_01_18_051650_create_invoice_payments_table', 1),
(24, '2020_01_20_091035_create_bill_payments_table', 1),
(25, '2020_02_25_052356_create_credit_notes_table', 1),
(26, '2020_02_26_033827_create_debit_notes_table', 1),
(27, '2020_03_12_095629_create_coupons_table', 1),
(28, '2020_03_12_120749_create_user_coupons_table', 1),
(29, '2020_04_02_045834_create_proposals_table', 1),
(30, '2020_04_02_055706_create_proposal_products_table', 1),
(31, '2020_04_18_035141_create_goals_table', 1),
(32, '2020_04_21_115823_create_assets_table', 1),
(33, '2020_04_24_023732_create_custom_fields_table', 1),
(34, '2020_04_24_024217_create_custom_field_values_table', 1),
(35, '2020_05_21_065337_create_permission_tables', 1),
(36, '2020_06_30_120854_add_balance_to_customers_table', 1),
(37, '2020_06_30_121531_add_balance_to_vender_table', 1),
(38, '2020_07_01_033457_change_product_services_tax_id_column_type', 1),
(39, '2020_07_01_063255_change_tax_column_type', 1),
(40, '2020_07_03_054342_add_convert_in_proposal_table', 1),
(41, '2020_07_06_102454_add_payment_type_in_orders_table', 1),
(42, '2020_07_07_052211_add_field_in_invoice_payments_table', 1),
(43, '2020_07_22_131715_change_amount_type_size', 1),
(44, '2020_08_04_034206_change_settings_value_type', 1),
(45, '2020_09_16_040822_change_user_type_size_in_users_table', 1),
(46, '2020_09_17_053350_change_shipping_default_val', 1),
(47, '2020_09_17_070031_add_description_field', 1),
(48, '2021_01_11_062508_create_chart_of_accounts_table', 1),
(49, '2021_01_11_070441_create_chart_of_account_types_table', 1),
(50, '2021_01_12_032834_create_journal_entries_table', 1),
(51, '2021_01_12_033815_create_journal_items_table', 1),
(52, '2021_01_20_072219_create_chart_of_account_sub_types_table', 1),
(53, '2021_06_15_035736_create_landing_page_sections_table', 1),
(54, '2021_07_15_091920_admin_payment_settings', 1),
(55, '2021_07_15_091933_company_payment_settings', 1),
(56, '2021_09_10_160559_add_last_login_to_user_table', 1),
(57, '2021_09_10_165514_create_plan_requests_table', 1),
(58, '2019_12_14_000001_create_personal_access_tokens_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\Customer', 1),
(2, 'App\\Models\\Customer', 2),
(2, 'App\\Models\\Customer', 3),
(2, 'App\\Models\\Customer', 4),
(2, 'App\\Models\\Customer', 5),
(3, 'App\\Models\\Vender', 1),
(4, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 5),
(4, 'App\\Models\\User', 6),
(4, 'App\\Models\\User', 7),
(4, 'App\\Models\\User', 8),
(5, 'App\\Models\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_exp_month` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_exp_year` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan_id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `price_currency` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txn_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Manually',
  `receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `account_id` int(11) NOT NULL,
  `vender_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `recurring` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'show dashboard', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'manage user', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'create user', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'edit user', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'delete user', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'create language', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'manage system settings', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'manage role', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'create role', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'edit role', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'delete role', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'manage permission', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'create permission', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'edit permission', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'delete permission', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'manage company settings', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'manage business settings', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'manage stripe settings', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'manage expense', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'create expense', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'edit expense', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'delete expense', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'manage invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'create invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'edit invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'delete invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'show invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'create payment invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'delete payment invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'send invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'delete invoice product', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'convert invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'manage plan', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'create plan', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'edit plan', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'manage constant unit', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'create constant unit', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'edit constant unit', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'delete constant unit', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'manage constant tax', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'create constant tax', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'edit constant tax', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'delete constant tax', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'manage constant category', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'create constant category', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'edit constant category', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'delete constant category', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'manage product & service', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'create product & service', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'edit product & service', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'delete product & service', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'manage customer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'create customer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'edit customer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'delete customer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'show customer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'manage vender', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'create vender', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'edit vender', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'delete vender', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'show vender', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'manage bank account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'create bank account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'edit bank account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'delete bank account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'manage transfer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'create transfer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'edit transfer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'delete transfer', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'manage constant payment method', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'create constant payment method', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'edit constant payment method', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'delete constant payment method', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'manage transaction', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'manage revenue', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'create revenue', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'edit revenue', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'delete revenue', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'manage bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'create bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'edit bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'delete bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'show bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'manage payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'create payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'edit payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'delete payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'delete bill product', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'buy plan', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'send bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'create payment bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'delete payment bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'manage order', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'income report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'expense report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'income vs expense report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'invoice report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'bill report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'tax report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'loss & profit report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'manage customer payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'manage customer transaction', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'manage customer invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'vender manage bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'manage vender bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'manage vender payment', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'manage vender transaction', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'manage credit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'create credit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'edit credit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'delete credit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'manage debit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'create debit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'edit debit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'delete debit note', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'duplicate invoice', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'duplicate bill', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'manage coupon', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'create coupon', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'edit coupon', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'delete coupon', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'manage proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'create proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'edit proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'delete proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'duplicate proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'show proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'send proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'delete proposal product', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'manage customer proposal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'manage goal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'create goal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'edit goal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'delete goal', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'manage assets', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'create assets', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'edit assets', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'delete assets', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'statement report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'manage constant custom field', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'create constant custom field', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'edit constant custom field', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'delete constant custom field', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'manage chart of account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'create chart of account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'edit chart of account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'delete chart of account', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'manage journal entry', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'create journal entry', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'edit journal entry', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'delete journal entry', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'show journal entry', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'balance sheet report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'ledger report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'trial balance report', 'web', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `duration` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_users` int(11) NOT NULL DEFAULT 0,
  `max_customers` int(11) NOT NULL DEFAULT 0,
  `max_venders` int(11) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `price`, `duration`, `max_users`, `max_customers`, `max_venders`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Free Plan', 0.00, 'Unlimited', 5, 5, 5, NULL, 'free_plan.png', '2021-12-20 05:32:40', '2021-12-20 05:32:40');

-- --------------------------------------------------------

--
-- Table structure for table `plan_requests`
--

CREATE TABLE `plan_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `duration` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'monthly',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_services`
--

CREATE TABLE `product_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_other` varchar(45) CHARACTER SET utf8 NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_price` double(20,2) NOT NULL DEFAULT 0.00,
  `purchase_price` double(20,2) NOT NULL DEFAULT 0.00,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `tax_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT 0,
  `unit_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_services`
--

INSERT INTO `product_services` (`id`, `name`, `name_other`, `sku`, `sale_price`, `purchase_price`, `quantity`, `tax_id`, `category_id`, `unit_id`, `type`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'new product', '', '123456', 100.00, 90.00, 0, '1', 1, 1, 'product', 'sdfsdf', 4, '2021-12-21 03:45:37', '2021-12-21 03:45:37'),
(2, 'product', '', '123456', 100.00, 90.00, 0, '1', 1, 1, 'product', 'dfgdfg', 4, '2021-12-21 04:03:32', '2021-12-21 04:03:32'),
(3, 'pen', 'قلم', 'etertrt', 123.00, 222.00, 0, '1', 1, 1, 'product', 'test', 4, '2021-12-21 05:12:33', '2021-12-21 05:15:30'),
(4, 'test product', 'منتج الاختبار', 'ytryt', 100.00, 99.98, 0, '1', 1, 1, 'product', 'hjuyiu', 4, '2021-12-28 05:50:52', '2021-12-28 05:50:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_service_categories`
--

CREATE TABLE `product_service_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#fc544b',
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_service_categories`
--

INSERT INTO `product_service_categories` (`id`, `name`, `type`, `color`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'new category', '0', 'FF381A', 4, '2021-12-21 03:36:52', '2021-12-21 03:36:52'),
(2, 'test', '0', 'FFFFFF', 4, '2021-12-21 03:53:46', '2021-12-21 03:53:46'),
(3, 'income category', '1', 'FFFFFF', 4, '2021-12-21 04:02:50', '2021-12-21 04:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `product_service_units`
--

CREATE TABLE `product_service_units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_service_units`
--

INSERT INTO `product_service_units` (`id`, `name`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'new unit', 4, '2021-12-21 03:37:50', '2021-12-21 03:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE `proposals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proposal_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `issue_date` date NOT NULL,
  `send_date` date DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `discount_apply` int(11) NOT NULL DEFAULT 0,
  `converted_invoice_id` int(11) NOT NULL DEFAULT 0,
  `is_convert` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proposal_products`
--

CREATE TABLE `proposal_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `tax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0.00',
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `revenues`
--

CREATE TABLE `revenues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `account_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'web', 0, '2021-12-20 05:32:40', '2021-12-20 05:32:40'),
(2, 'customer', 'web', 0, '2021-12-20 05:32:41', '2021-12-20 05:32:41'),
(3, 'vender', 'web', 0, '2021-12-20 05:32:41', '2021-12-20 05:32:41'),
(4, 'company', 'web', 1, '2021-12-20 05:32:41', '2021-12-20 05:32:41'),
(5, 'accountant', 'web', 2, '2021-12-20 05:32:42', '2021-12-20 05:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 4),
(1, 5),
(2, 1),
(2, 4),
(3, 1),
(3, 4),
(4, 1),
(4, 4),
(5, 1),
(5, 4),
(6, 1),
(7, 1),
(8, 1),
(8, 4),
(9, 1),
(9, 4),
(10, 1),
(10, 4),
(11, 1),
(11, 4),
(12, 1),
(12, 4),
(13, 1),
(13, 4),
(14, 1),
(14, 4),
(15, 1),
(15, 4),
(16, 4),
(17, 4),
(18, 1),
(19, 4),
(19, 5),
(20, 4),
(20, 5),
(21, 4),
(21, 5),
(22, 4),
(22, 5),
(23, 4),
(23, 5),
(24, 4),
(24, 5),
(25, 4),
(25, 5),
(26, 4),
(26, 5),
(27, 2),
(27, 4),
(27, 5),
(28, 4),
(28, 5),
(29, 4),
(29, 5),
(30, 4),
(30, 5),
(31, 4),
(31, 5),
(32, 4),
(32, 5),
(33, 1),
(33, 4),
(34, 1),
(35, 1),
(36, 4),
(36, 5),
(37, 4),
(37, 5),
(38, 4),
(38, 5),
(39, 4),
(39, 5),
(40, 4),
(40, 5),
(41, 4),
(41, 5),
(42, 4),
(42, 5),
(43, 4),
(43, 5),
(44, 4),
(44, 5),
(45, 4),
(45, 5),
(46, 4),
(46, 5),
(47, 4),
(47, 5),
(48, 4),
(48, 5),
(49, 4),
(49, 5),
(50, 4),
(50, 5),
(51, 4),
(51, 5),
(52, 4),
(52, 5),
(53, 4),
(53, 5),
(54, 4),
(54, 5),
(55, 4),
(55, 5),
(56, 2),
(56, 4),
(56, 5),
(57, 4),
(57, 5),
(58, 4),
(58, 5),
(59, 4),
(59, 5),
(60, 4),
(60, 5),
(61, 3),
(61, 4),
(61, 5),
(62, 4),
(62, 5),
(63, 4),
(63, 5),
(64, 4),
(64, 5),
(65, 4),
(65, 5),
(66, 4),
(66, 5),
(67, 4),
(67, 5),
(68, 4),
(68, 5),
(69, 4),
(69, 5),
(74, 4),
(74, 5),
(75, 4),
(75, 5),
(76, 4),
(76, 5),
(77, 4),
(77, 5),
(78, 4),
(78, 5),
(79, 4),
(79, 5),
(80, 4),
(80, 5),
(81, 4),
(81, 5),
(82, 4),
(82, 5),
(83, 3),
(83, 4),
(83, 5),
(84, 4),
(84, 5),
(85, 4),
(85, 5),
(86, 4),
(86, 5),
(87, 4),
(87, 5),
(88, 4),
(88, 5),
(89, 4),
(90, 4),
(90, 5),
(91, 4),
(91, 5),
(92, 4),
(92, 5),
(93, 1),
(93, 4),
(94, 4),
(94, 5),
(95, 4),
(95, 5),
(96, 4),
(96, 5),
(97, 4),
(97, 5),
(98, 4),
(98, 5),
(99, 4),
(99, 5),
(100, 4),
(100, 5),
(101, 2),
(102, 2),
(103, 2),
(104, 3),
(105, 3),
(106, 3),
(107, 3),
(108, 4),
(108, 5),
(109, 4),
(109, 5),
(110, 4),
(110, 5),
(111, 4),
(111, 5),
(112, 4),
(112, 5),
(113, 4),
(113, 5),
(114, 4),
(114, 5),
(115, 4),
(115, 5),
(116, 4),
(117, 4),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 4),
(122, 5),
(123, 4),
(123, 5),
(124, 4),
(124, 5),
(125, 4),
(125, 5),
(126, 4),
(126, 5),
(127, 2),
(127, 4),
(127, 5),
(128, 4),
(128, 5),
(129, 4),
(129, 5),
(130, 2),
(131, 4),
(131, 5),
(132, 4),
(132, 5),
(133, 4),
(133, 5),
(134, 4),
(134, 5),
(135, 4),
(135, 5),
(136, 4),
(136, 5),
(137, 4),
(137, 5),
(138, 4),
(138, 5),
(139, 4),
(139, 5),
(140, 4),
(140, 5),
(141, 4),
(141, 5),
(142, 4),
(142, 5),
(143, 4),
(143, 5),
(144, 4),
(144, 5),
(145, 4),
(145, 5),
(146, 4),
(146, 5),
(147, 4),
(147, 5),
(148, 4),
(148, 5),
(149, 4),
(149, 5),
(150, 4),
(150, 5),
(151, 4),
(151, 5),
(152, 4),
(152, 5),
(153, 4),
(153, 5),
(154, 4),
(154, 5),
(155, 4),
(155, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(45) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL,
  `data` blob NOT NULL,
  `payload` varchar(40) NOT NULL,
  `last_activity` varchar(45) NOT NULL,
  `user_id` varchar(45) NOT NULL,
  `user_agent` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'invoice_template', 'template13', 4, NULL, NULL),
(2, 'invoice_color', 'ffffff', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `name`, `rate`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'new tax', '10', 4, '2021-12-21 03:38:23', '2021-12-21 03:38:23');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `payment_id` int(11) NOT NULL DEFAULT 0,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transfers`
--

CREATE TABLE `transfers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_account` int(11) NOT NULL DEFAULT 0,
  `to_account` int(11) NOT NULL DEFAULT 0,
  `amount` double(15,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `payment_method` int(11) NOT NULL DEFAULT 0,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_no` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `plan` int(11) DEFAULT NULL,
  `plan_expire_date` date DEFAULT NULL,
  `requested_plan` int(11) NOT NULL DEFAULT 0,
  `delete_status` int(11) NOT NULL DEFAULT 1,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `vat_no`, `email`, `email_verified_at`, `password`, `type`, `avatar`, `lang`, `created_by`, `plan`, `plan_expire_date`, `requested_plan`, `delete_status`, `is_active`, `remember_token`, `last_login_at`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '', 'superadmin@example.com', NULL, '$2y$10$gkNuEkyCnk6p2ek/ODiIUu8U8WqbFLGaCj27MZL7jOsf6IOOASlv2', 'super admin', '', 'en', 0, NULL, NULL, 0, 1, 1, NULL, NULL, '2021-12-20 05:32:41', '2021-12-20 05:32:41'),
(2, 'company', '', 'company@example.com', NULL, '$2y$10$ngzr01zEgVm6/twBbvK7N.8NcxienAkvRMuw/iwlrfUWDnP17Z2Ti', 'company', '', 'en', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-20 05:32:42', '2021-12-20 05:32:42'),
(3, 'accountant', '', 'accountant@example.com', NULL, '$2y$10$eMIVvM0xMfKHFG8zqwLV9eb/I0ziM9ggQYg.JKRPxrB/xVH7AFgd.', 'accountant', '', 'en', 2, NULL, NULL, 0, 1, 1, NULL, NULL, '2021-12-20 05:32:43', '2021-12-20 05:32:43'),
(4, 'harsha', '123456789', 'harsha.4harsha@gmail.com', NULL, '$2y$10$65HdAziylp3Qh9R2f79jxe1QZdJ9tczN1fFh3nC.X4FXU4JOMbzMS', 'company', NULL, 'en', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-21 03:09:39', '2021-12-26 23:51:56'),
(5, 'testuser', '', 'test@gmail.com', NULL, '$2y$10$DDr2Zk1S97/UaTM4gquNXeyWbfCXTq8yHE6AVoXXAFjH8Jt7/TVS6', 'company', NULL, '', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-24 03:38:05', '2021-12-24 03:38:05'),
(6, 'helan', '', 'abc@gmail.com', NULL, '$2y$10$HHMDxndIsZaD1Vf1Yb184.OhP2S2DRJYk3K6i9LkknhXXXqcEkrnW', 'company', NULL, 'en', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-27 00:19:42', '2021-12-27 00:19:42'),
(7, 'sdfdsf', '', 'abcd@gmail.com', NULL, '$2y$10$VpBEU0cM1cZtIDT.3FSSB.mVXcjjwBOPXfFSEEZtkfK0Ne5o9cbHi', 'company', NULL, 'en', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-27 00:21:07', '2021-12-27 00:21:07'),
(8, 'dfgfdgfg', '123123123213', 'abg@gmail.com', NULL, '$2y$10$Ci8z8eYaqnj0ycdUvW3hs.LGQjk.u05cZDwn4RhdWg7uqwWupKxei', 'company', NULL, 'en', 1, 1, NULL, 0, 1, 1, NULL, NULL, '2021-12-27 00:23:48', '2021-12-27 00:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_coupons`
--

CREATE TABLE `user_coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `coupon` int(11) NOT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venders`
--

CREATE TABLE `venders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vender_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(11) NOT NULL DEFAULT 0,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `billing_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `balance` double(8,2) NOT NULL DEFAULT 0.00,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `venders`
--

INSERT INTO `venders` (`id`, `vender_id`, `name`, `email`, `password`, `contact`, `avatar`, `created_by`, `is_active`, `email_verified_at`, `billing_name`, `billing_country`, `billing_state`, `billing_city`, `billing_phone`, `billing_zip`, `billing_address`, `shipping_name`, `shipping_country`, `shipping_state`, `shipping_city`, `shipping_phone`, `shipping_zip`, `shipping_address`, `lang`, `balance`, `remember_token`, `last_login_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'helan', 'helan@gmail.com', '$2y$10$0IWNBWvmnAEDHeBgbBQ/..IFH7OQdSV4N0kOTudw2ziMbI9Uu8MbO', '8281066602', '', 4, 1, NULL, 'helan', 'India', 'kerala', 'calicut', '08281066602', '673620', 'helan villa', 'helan', 'India', 'kerala', 'calicut', '08281066602', '673620', 'helan villa', '', 0.00, NULL, NULL, '2021-12-24 01:01:27', '2021-12-24 01:01:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_payment_settings`
--
ALTER TABLE `admin_payment_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_payment_settings_name_created_by_unique` (`name`,`created_by`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_payments`
--
ALTER TABLE `bill_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_products`
--
ALTER TABLE `bill_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chart_of_accounts`
--
ALTER TABLE `chart_of_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chart_of_account_sub_types`
--
ALTER TABLE `chart_of_account_sub_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chart_of_account_types`
--
ALTER TABLE `chart_of_account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_payment_settings`
--
ALTER TABLE `company_payment_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_payment_settings_name_created_by_unique` (`name`,`created_by`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_notes`
--
ALTER TABLE `credit_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `custom_field_values_record_id_field_id_unique` (`record_id`,`field_id`),
  ADD KEY `custom_field_values_field_id_foreign` (`field_id`);

--
-- Indexes for table `debit_notes`
--
ALTER TABLE `debit_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goals`
--
ALTER TABLE `goals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_products`
--
ALTER TABLE `invoice_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_entries`
--
ALTER TABLE `journal_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_items`
--
ALTER TABLE `journal_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_page_sections`
--
ALTER TABLE `landing_page_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_order_id_unique` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plans_name_unique` (`name`);

--
-- Indexes for table `plan_requests`
--
ALTER TABLE `plan_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_services`
--
ALTER TABLE `product_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_service_categories`
--
ALTER TABLE `product_service_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_service_units`
--
ALTER TABLE `product_service_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposal_products`
--
ALTER TABLE `proposal_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revenues`
--
ALTER TABLE `revenues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_created_by_unique` (`name`,`created_by`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfers`
--
ALTER TABLE `transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_coupons`
--
ALTER TABLE `user_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venders`
--
ALTER TABLE `venders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `venders_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_payment_settings`
--
ALTER TABLE `admin_payment_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bill_payments`
--
ALTER TABLE `bill_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bill_products`
--
ALTER TABLE `bill_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chart_of_accounts`
--
ALTER TABLE `chart_of_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `chart_of_account_sub_types`
--
ALTER TABLE `chart_of_account_sub_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `chart_of_account_types`
--
ALTER TABLE `chart_of_account_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `company_payment_settings`
--
ALTER TABLE `company_payment_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_notes`
--
ALTER TABLE `credit_notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `debit_notes`
--
ALTER TABLE `debit_notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goals`
--
ALTER TABLE `goals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_products`
--
ALTER TABLE `invoice_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `journal_entries`
--
ALTER TABLE `journal_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `journal_items`
--
ALTER TABLE `journal_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `landing_page_sections`
--
ALTER TABLE `landing_page_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plan_requests`
--
ALTER TABLE `plan_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_services`
--
ALTER TABLE `product_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_service_categories`
--
ALTER TABLE `product_service_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_service_units`
--
ALTER TABLE `product_service_units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `proposals`
--
ALTER TABLE `proposals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proposal_products`
--
ALTER TABLE `proposal_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revenues`
--
ALTER TABLE `revenues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transfers`
--
ALTER TABLE `transfers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_coupons`
--
ALTER TABLE `user_coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venders`
--
ALTER TABLE `venders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD CONSTRAINT `custom_field_values_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
